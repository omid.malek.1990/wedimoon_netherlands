<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 4.7.4
 */

/**
 * Database `wedimoon_birde`
 */

/* `wedimoon_birde`.`customers` */
$customers = array(
  array('id' => '1','fullname' => '(1)نام کاربر','email' => 'sample@gmail.com','email_verified_at' => '2019-01-20 10:22:00','mobile' => '093656475851','password' => '25f9e794323b453885f5181f1b624d0b','citie_id' => '1','active' => '1','remember_token' => NULL,'created_at' => '2019-01-20 10:22:53','updated_at' => '2019-01-20 10:22:53'),
  array('id' => '2','fullname' => '(2)نام کاربر','email' => 'sample@gmail.com','email_verified_at' => '2019-01-20 10:22:00','mobile' => '093656475852','password' => '25f9e794323b453885f5181f1b624d0b','citie_id' => '2','active' => '1','remember_token' => NULL,'created_at' => '2019-01-20 10:22:53','updated_at' => '2019-01-20 10:22:53'),
  array('id' => '3','fullname' => 'امید مالک','email' => '','email_verified_at' => '2019-01-21 15:37:12','mobile' => '09157247585','password' => '25f9e794323b453885f5181f1b624d0b','citie_id' => '2','active' => '1','remember_token' => NULL,'created_at' => '2019-01-21 12:07:12','updated_at' => '2019-01-21 12:07:12'),
  array('id' => '4','fullname' => 'نازنین خزاعی','email' => '','email_verified_at' => '2019-02-26 15:08:21','mobile' => '09031310230','password' => '059d9e01176ab2f0892fe2215835bf19','citie_id' => '2','active' => '1','remember_token' => NULL,'created_at' => '2019-02-26 11:38:21','updated_at' => '2019-02-26 11:38:21')
);
