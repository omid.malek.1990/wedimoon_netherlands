<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 4.7.4
 */

/**
 * Database `wedimoon_birde`
 */

/* `wedimoon_birde`.`cities` */
$cities = array(
  array('id' => '1','name' => 'تهران','visible' => '1','created_at' => '2019-01-20 10:21:17','updated_at' => '2019-01-20 10:21:17'),
  array('id' => '2','name' => 'مشهد','visible' => '1','created_at' => '2019-01-20 10:21:17','updated_at' => '2019-01-20 10:21:17')
);
