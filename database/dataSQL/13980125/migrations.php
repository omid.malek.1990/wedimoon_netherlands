<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 4.7.4
 */

/**
 * Database `wedimoon_birde`
 */

/* `wedimoon_birde`.`migrations` */
$migrations = array(
  array('id' => '1','migration' => '2014_10_12_000000_create_users_table','batch' => '1'),
  array('id' => '2','migration' => '2014_10_12_100000_create_password_resets_table','batch' => '1'),
  array('id' => '3','migration' => '2016_06_01_000001_create_oauth_auth_codes_table','batch' => '1'),
  array('id' => '4','migration' => '2016_06_01_000002_create_oauth_access_tokens_table','batch' => '1'),
  array('id' => '5','migration' => '2016_06_01_000003_create_oauth_refresh_tokens_table','batch' => '1'),
  array('id' => '6','migration' => '2016_06_01_000004_create_oauth_clients_table','batch' => '1'),
  array('id' => '7','migration' => '2016_06_01_000005_create_oauth_personal_access_clients_table','batch' => '1'),
  array('id' => '8','migration' => '2018_10_30_234739_create_categories_table','batch' => '1'),
  array('id' => '9','migration' => '2018_11_17_103616_create_comments_table','batch' => '1'),
  array('id' => '10','migration' => '2018_11_17_172041_create_posts_table','batch' => '1'),
  array('id' => '11','migration' => '2018_11_17_210818_create_images_table','batch' => '1'),
  array('id' => '12','migration' => '2018_11_19_103712_create_cities_table','batch' => '1'),
  array('id' => '13','migration' => '2018_11_19_180553_create_customers_table','batch' => '1'),
  array('id' => '14','migration' => '2018_12_11_080602_create_admins_table','batch' => '1'),
  array('id' => '15','migration' => '2019_01_18_101048_create_likes_table','batch' => '1'),
  array('id' => '16','migration' => '2019_01_18_101146_create_scores_table','batch' => '1'),
  array('id' => '17','migration' => '2019_01_18_102155_create_bookmarks_table','batch' => '1'),
  array('id' => '18','migration' => '2019_03_06_214341_add_services_users_table','batch' => '2')
);
