<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 4.7.4
 */

/**
 * Database `wedimoon_birde`
 */

/* `wedimoon_birde`.`categories` */
$categories = array(
  array('id' => '1','title' => 'طلا و جواهرات','visible' => '1','created_at' => '2019-01-20 10:20:54','updated_at' => '2019-01-20 10:20:54'),
  array('id' => '2','title' => 'آرایش عروس','visible' => '1','created_at' => '2019-01-20 10:20:54','updated_at' => '2019-01-20 10:20:54'),
  array('id' => '3','title' => 'پیرایش داماد','visible' => '1','created_at' => '2019-01-20 10:20:54','updated_at' => '2019-01-20 10:20:54'),
  array('id' => '4','title' => 'کت و شلوار','visible' => '1','created_at' => '2019-01-20 10:20:54','updated_at' => '2019-01-20 10:20:54'),
  array('id' => '5','title' => 'تشریفات','visible' => '1','created_at' => '2019-01-20 10:20:54','updated_at' => '2019-01-20 10:20:54'),
  array('id' => '6','title' => 'مزون','visible' => '1','created_at' => '2019-01-20 10:20:54','updated_at' => '2019-01-20 10:20:54'),
  array('id' => '7','title' => 'آتلیه','visible' => '1','created_at' => '2019-01-20 10:20:54','updated_at' => '2019-01-20 10:20:54'),
  array('id' => '8','title' => 'تالار','visible' => '1','created_at' => '2019-01-20 10:20:54','updated_at' => '2019-01-20 10:20:54'),
  array('id' => '9','title' => 'گل','visible' => '1','created_at' => '2019-01-20 10:20:54','updated_at' => '2019-01-20 10:20:54'),
  array('id' => '10','title' => 'سایر','visible' => '1','created_at' => '2019-01-20 10:20:54','updated_at' => '2019-01-20 10:20:54')
);
