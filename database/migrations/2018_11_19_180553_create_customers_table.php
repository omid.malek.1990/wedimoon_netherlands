<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname')->comment('نام و نام خانوادگی مجموعه دار');
            $table->string('email')->comment('ایمیل مجموعه دار');
            $table->timestamp('email_verified_at');
            $table->string('mobile',15)->comment('شماره تلفن همراه مجموعه دار')->nullable();
            $table->string('password',100)->comment('کلمه عبور مجموعه دار');
            $table->integer('citie_id')->unsigned()->index()->nullable()->comment('کلید خارجی شهر');
            $table->boolean('active')->comment('وضعیت نمایش و یا عدم نمایش نمایش نظر')
                ->default(true);
            $table->rememberToken();
            $table->timestamps();
        });


        Schema::table('comments',function (Blueprint $table){
            $table->foreign('customer_id')->references('id')->on('customers')
                ->onDelete('RESTRICT')->onUpdate('cascade');
        });

        Schema::table('customers',function (Blueprint $table){
            $table->foreign('citie_id')->references('id')->on('cities')
                ->onDelete('RESTRICT')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
