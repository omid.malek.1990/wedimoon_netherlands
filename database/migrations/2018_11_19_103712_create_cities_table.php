<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',60);
            $table->boolean('visible')->comment('وضعیت نمایش و یا عدم نمایش نمایش نظر')
                ->default(true);
            $table->timestamps();
        });

        Schema::table('users',function (Blueprint $table){
            $table->foreign('citie_id')->references('id')->on('cities')
                ->onDelete('RESTRICT')->onUpdate('cascade');
        });
        Schema::table('posts',function (Blueprint $table){
            $table->foreign('citie_id')->references('id')->on('cities')
                ->onDelete('RESTRICT')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
