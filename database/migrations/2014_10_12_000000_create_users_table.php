<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('fullname')->comment('نام و نام خانوادگی مجموعه دار');
            $table->string('email')->comment('ایمیل مجموعه دار');
            $table->timestamp('email_verified_at');
            $table->string('phone',15)->comment('شماره تلفن ثابت مجموعه دار')->nullable();
            $table->string('mobile',15)->comment('شماره تلفن همراه مجموعه دار')->nullable();
            $table->string('collection_title',80)->comment('عنوان خدمت مجموعه دار');
            $table->string('password',100)->comment('کلمه عبور مجموعه دار');
            $table->string('description',2000)->comment('توضیحات');
            $table->string('services',2000)->comment('خدمات');
            $table->string('address',300)->comment('آدرس مجموعه');
            $table->string('long_itude',50)->comment('طول جغرافیایی');
            $table->string('late_itude',50)->comment('عرض جغرافیایی');
            $table->integer('categorie_id')->unsigned()->index()->nullable()->comment('کلید خارجی دسته بندی خدمات');
            $table->integer('citie_id')->unsigned()->index()->nullable()->comment('کلید خارجی شهر');
            $table->boolean('active')->comment('وضعیت نمایش و یا عدم نمایش نمایش نظر')->default(0)
                ->default(true);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
