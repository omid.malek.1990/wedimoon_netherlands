<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('text',400)->comment('متن نظر کاربران');
            $table->integer('customer_id')->unsigned()->index()->nullable()->comment('کلید خارجی کاربر');
            $table->integer('post_id')->unsigned()->index()->nullable()->comment('کلید خارجی پست');
            $table->boolean('visible')->comment('وضعیت نمایش و یا عدم نمایش نمایش نظر')
                ->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
