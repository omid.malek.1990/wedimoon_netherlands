<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookmarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookmarks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned()->index()->nullable()->comment('کلید خارجی کاربر');
            $table->integer('post_id')->unsigned()->index()->nullable()->comment('کلید خارجی پست');
            $table->integer('user_id')->unsigned()->index()->nullable()->comment('کلید خارجی مجموعه دار');
            $table->timestamps();
        });


        Schema::table('bookmarks',function (Blueprint $table){
            $table->foreign('customer_id')->references('id')->on('customers')
                ->onDelete('RESTRICT')->onUpdate('cascade');

            $table->foreign('post_id')->references('id')->on('posts')
                ->onDelete('RESTRICT')->onUpdate('cascade');


            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('RESTRICT')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookmarks');
    }
}
