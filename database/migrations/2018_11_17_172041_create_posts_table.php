<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',60)->comment('عنوان پست');
            $table->string('description',600)->comment('توضیحات پست');
            $table->integer('user_id')->unsigned()->index()->nullable()->comment('کلید خارجی کاربر');
            $table->integer('categorie_id')->unsigned()->index()->nullable()->comment('کلید خارجی کاربر');
            $table->integer('citie_id')->unsigned()->index()->nullable()->comment('کلید خارجی شهر');
            $table->integer('number_visited')->default(0)->comment('تعداد کاربران بازدید کرده');
            $table->boolean('special_offer')->default(false)->comment('پیشنهادات ویژه');
            $table->integer('likes')->default(0)->comment('جهت استفاده در قسمت محبوبترین');
            $table->integer('score')->default(0)->comment('امتیازات');
            $table->boolean('visible')->comment('وضعیت نمایش و یا عدم نمایش نمایش نظر')
                ->default(true);
            $table->timestamps();
        });

        Schema::table('posts',function(Blueprint $table){
            $table->foreign('categorie_id')->references('id')->on('categories')->onDelete('RESTRICT')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('RESTRICT')->onUpdate('cascade');
        });


        Schema::table('comments',function (Blueprint $table){
            $table->foreign('post_id')->references('id')->on('posts')
                ->onDelete('RESTRICT')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
