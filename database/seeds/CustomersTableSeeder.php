<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\Hash;
use App\Citie;
use App\Customer;
class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $encrypted = md5('123456789');
        $Cities = Citie::all();
        for ($i = 0; $i < count($Cities); $i++) {
            $data = [
                'fullname' => '(' . ($i + 1) . ')' . 'نام کاربر',
                'email' => 'sample@gmail.com',
                'email_verified_at' => date('Y-m-d H:i:S'),
                'mobile' => '09365647585' . ($i + 1),
                'password' => $encrypted,
                'active' => true,
                'citie_id' => $Cities[$i]->id
            ];
            Customer::create($data);
        }
    }
}
