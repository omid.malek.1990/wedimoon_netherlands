<?php

use Illuminate\Database\Seeder;

use App\Citie;
class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data=[
            ['name'=>'تهران'],
            ['name'=>'مشهد'],
        ];
        foreach ($data as $item){
            $Answer=Citie::create($item);
        }
    }
}

