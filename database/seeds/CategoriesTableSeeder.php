<?php

use Illuminate\Database\Seeder;

use App\Categorie;
use Illuminate\Hashing;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data=[
            ['title'=>'طلا و جواهرات'],
            ['title'=>'آرایش عروس'],
            ['title'=>'پیرایش داماد'],
            ['title'=>'کت و شلوار'],
            ['title'=>'تشریفات'],
            ['title'=>'مزون'],
            ['title'=>'آتلیه'],
            ['title'=>'تالار'],
            ['title'=>'گل'],
            ['title'=>'سایر'],
        ];
        foreach ($data as $item){
            $Answer=Categorie::create($item);
        }

    }
}