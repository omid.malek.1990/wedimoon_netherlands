<?php

use Illuminate\Database\Seeder;

use App\Categorie;
use App\Post;
use App\User;
use App\Citie;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Categories = Categorie::all();
        $Cities = Citie::all();
        $StartUser = 0;
        $Users = User::all();
        for ($i = $StartUser; $i < count($Users); $i++) {
                $data = [
                    'title' => '(' . ($i + 1) . ')' . 'عنوان پست',
                    'description' => 'description of post',
                    'user_id' => $Users[$i]->id,
                    'citie_id' => $Users[$i]->citie_id,
                    'categorie_id' => $Users[$i]->categorie_id,
                    'number_visited' => $i,
                    'special_offer' => false,
                    'likes' => 0,
                ];
                $Post = Post::create($data);
            }
    }
}
