<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use App\Services\Collections\Token;
use App\Categorie;
use App\User;
use App\Citie;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    protected $token;

    public function __construct(Token $token)
    {
        $this->token = $token;
    }



    public function run()
    {
        $encrypted = md5('123456789');
        $Categories = Categorie::all();
        $Cities = Citie::all();
        $CategoriesNumber=0;
        for ($j = 0; $j < count($Cities); $j++) {
            for ($i = $CategoriesNumber; $i < count($Categories); $i++) {
                for($k=$i,$h=0;$h<10;$k++,$h++){
                    $data = [
                        'fullname' => '(' . ($k + 1) . ')' . 'نام مسئول مجموعه',
                        'email' => 'sample@gmail.com',
                        'email_verified_at' => date('Y-m-d H:i:S'),
                        'phone' => '05138652222' . ($k + 1),
                        'mobile' => '09365647585' . ($k + 1),
                        'collection_title' => '(' . ($k + 1) . ')' . 'مجموعه دار',
                        'address' => '(' . ($k + 1) . ')' . 'آدرس مجموعه مورد نظر',
                        'password' => $encrypted,
                        'long_itude' => '36.3281178',
                        'late_itude' => '59.5455583',
                        'categorie_id' => $Categories[$i]['id'],
                        'active' => true,
                        'citie_id' => $Cities[$j]->id
                    ];
                    $User = User::create($data);
                    $token = $this->token->TokenCollectionCreator($User->id);
                }
            }
        }
    }
}
