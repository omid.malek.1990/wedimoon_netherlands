<?php

use Illuminate\Database\Seeder;

use App\Post;
use App\Image;
use App\Categorie;
class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Post = Post::all();
        $data = [
            [
                'src' => 'baner1.jpg' ,
                'record_id' => 0,
                'used_in' => 'index.app',
            ],
            [
                'src' => 'baner2.jpg' ,
                'record_id' => 0,
                'used_in' => 'index.app',
            ],
            [
                'src' => 'baner3.jpg' ,
                'record_id' => 0,
                'used_in' => 'index.app',
            ],
            [
                'src' => 'baner4.jpg' ,
                'record_id' => 0,
                'used_in' => 'index.app',
            ]
        ];
        for ($i=0;$i<3;$i++){
            $Image = Image::create($data[$i]);
        }
        for ($i = 0; $i < count($Post); $i++) {
            $data = [
                'src' => 'MdKeyboard.jpg' ,
                'record_id' => $Post[$i]->id,
                'used_in' => 'logo.post',
            ];
            $Image = Image::create($data);

            for($j=0;$j<3;$j++){
                $data = [
                    'src' => 'SmMessageKeyboard.jpg' ,
                    'record_id' => $Post[$i]->id,
                    'used_in' => 'slider.post',
                ];
                $Image = Image::create($data);
            }
        }
        $Categories=Categorie::all();
        foreach ($Categories as $categorie){
            $data = [
                'src' => 'rose-shape.png' ,
                'record_id' => $categorie->id,
                'used_in' => 'icon.categorie',
            ];
            $Image = Image::create($data);
        }

    }
}
