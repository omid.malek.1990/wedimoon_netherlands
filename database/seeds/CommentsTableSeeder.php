<?php

use Illuminate\Database\Seeder;

use App\Post;
use App\Citie;
use App\Comment;
use App\Customer;
class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Posts = Post::all();
        for($j=0;$j<count($Posts);$j++){
            $Users=Customer::where('citie_id','=',$Posts[$j]->citie_id)->get();
            $data = [
                'text' => '('. ($j+1).')'.'متن کامنت ' ,
                'customer_id' => $Users->first()->id,
                'post_id' => $Posts[$j]->id,
                'visible' => false,
            ];
            Comment::create($data);
        }
    }
}
