<!DOCTYPE html>
<html class="rtl" dir="rtl" lang="fa">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link  rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}" />
    <title>@yield('title')</title>
    @stack('styles')
</head>
<body dir="ltr">
    <div class="container-fluid Container">
        @yield('content')
    </div>
</body>
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/all.js') }}"></script>
@stack('scripts')
</html>
