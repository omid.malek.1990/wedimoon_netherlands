<!DOCTYPE html>
<html class="rtl" dir="rtl" lang="fa">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link  rel="stylesheet" type="text/css" href="{{ asset('css/admin.css') }}" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <title>@yield('title')</title>
    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--}}
</head>
<body dir="ltr">
<div class="container-fluid Container">
    @yield('content')
</div>
<script type="text/javascript" src="{{ asset('js/alladmin.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/AdminApp.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/fontawesome/all.js') }}"></script>
</body>
</html>
