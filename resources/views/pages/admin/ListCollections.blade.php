@extends('pages.admin.dashboard')

@section('Items')
    <ul id="ListGroup" class="list-group ListGroupItems" item="{{ $operation }}">
        <li id="InsertOperation" class="list-group-item">
            <a href="{{ route('posts.create') }}">افزودن مجموعه</a>
        </li>
        <li id="InsertOperation" class="list-group-item">
            <a href="{{ route('collections.index') }}">مجموعه داران ثبت نشده</a>
        </li>
        <li id="ListOperation" class="list-group-item">
            <a href="{{ route('posts.index') }}">لیست مجموعه ها</a>
        </li>
    </ul>
@endsection

@section('BaseSection')

    <div class="SectionMajor table-responsive">

        @php
            if(empty($collections)){
        @endphp
        <section  class="ErrorFrame">
            <section class="alert alert-primary text-center ErrorBox BMitra">
                <span class="IRanSans ErrorMessage">{{ 'هیچ داده ای یافت نشد' }}</span>
            </section>
        </section>
        @php
            }else{
        @endphp

        <table class="table">
            <thead class="thead-light">
            <tr>
                <th>مجموعه</th>
                <th>دسته</th>
                <th>شهر</th>
                <th>وضعیت</th>
                <th>عملیات</th>
            </tr>
            </thead>
            <tbody>
            @foreach($collections as $collection)
                <tr>
                    <td>{{ $collection->user->collection_title }}</td>
                    <td>{{ $collection->categorie->title }}</td>
                    <td>{{ $collection->citie->name }}</td>
                    <td>{!! $collection->active !!}</td>
                    <td>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <button type="button" class="btn btn-outline-secondary dropdown-toggle"
                                        data-toggle="dropdown">
                                    انتخاب کنید
                                </button>
                                <div class="dropdown-menu">
                                    <a data-toggle="modal" data-target="#RemoveModal" class="dropdown-item DropDownItem"
                                       path="{{ route('post.remove',['id'=>$collection->id]) }}"
                                       href="#">
                                        <span class="fas fa-trash-alt"></span>
                                        <span>حذف</span>
                                    </a>
                                    <a class="dropdown-item DropDownItem" href="{{ route('posts.specialOffer',['id'=>$collection->id]) }}">
                                        <span class="fas fa-splotch"></span>
                                        <span>
                                            @php echo (($collection->special_offer==0) ? 'فعال کردن پیشنهاد ویژه':'غیرفعال کردن پیشنهاد ویژه') @endphp
                                        </span>
                                    </a>
                                    <a class="dropdown-item DropDownItem" href="{{ route('posts.edit',['id'=>$collection->id]) }}">
                                        <span class="fas fa-edit DropdDownIcon"></span>
                                        <span class="">ویرایش</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if($collections->total()>$paginate)
            <div class="FooterPagination">
                <span class="PaginationLinks">{{ $collections->links() }}</span>
            </div>
        @endif


        @php
            }
        @endphp


        @includeIf('pages.admin.removeModalUser')


    </div>
@endsection