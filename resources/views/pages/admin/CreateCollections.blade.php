@extends('pages.admin.dashboard')

@section('Items')
    <ul id="ListGroup" class="list-group ListGroupItems" item="{{ $operation }}">
        <li id="InsertOperation" class="list-group-item">
            <a href="{{ route('posts.create') }}">افزودن مجموعه</a>
        </li>
        <li id="ReservedList" class="list-group-item">
            <a href="{{ route('collections.index') }}">مجموعه داران ثبت نشده</a>
        </li>
        <li id="ListOperation" class="list-group-item">
            <a href="{{ route('posts.index') }}">لیست مجموعه ها</a>
        </li>
    </ul>
@endsection

@section('BaseSection')
    <div class="CreateCollectionPage">
        @includeIf('pages.error')
        <form action="{{ route('posts.store') }}" method="post">
            {{ csrf_field() }}
            <div class="CollectionInfo">
                <div class="InputFrame">
                    <label for="Fullname">نام مجموعه دار</label>
                    <input type="text" class="form-control text-right" name="Fullname"/>
                </div>
                <div class="InputFrame">
                    <label for="Phone">تلفن ثابت</label>
                    <input type="text" class="form-control text-left" name="Phone"/>
                </div>
                <div class="InputFrame">
                    <label for="Longitude">طول جغرافیایی</label>
                    <input type="text" class="form-control text-left" name="Longitude"/>
                </div>
                <div class="InputFrame">
                    <label for="Mobile">تلفن همراه</label>
                    <input type="text" class="form-control text-left" name="Mobile"/>
                </div>
                <div class="InputFrame">
                    <label for="Category">دسته بندی</label>
                    <select dir="rtl" class="form-control" name="Category" id="Category">
                        @foreach($categories as $Item)
                            <option value="{{ $Item->id }}">{{ $Item->title }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="InputFrame">
                    <label for="Address">نشانی مجموعه</label>
                    <textarea name="Address" rows="10" class="form-control text-right"></textarea>
                </div>
            </div>
            <div class="CollectionPost">
                <div class="InputFrame">
                    <label for="TitleCollection">عنوان مجموعه</label>
                    <input type="text" class="form-control text-right" name="TitleCollection"/>
                </div>
                <div class="InputFrame">
                    <label for="Email">آدرس ایمیل</label>
                    <input type="text" class="form-control text-left" name="Email"/>
                </div>
                <div class="InputFrame">
                    <label for="Lateitude">عرض جغرافیایی</label>
                    <input type="text" class="form-control text-left" name="Lateitude"/>
                </div>
                <div class="InputFrame">
                    <label for="PostTitle">نام محصول</label>
                    <input type="text" class="form-control text-left" name="PostTitle"/>
                </div>
                <div class="InputFrame">
                    <label for="PostTitle">نام شهر</label>
                    <select dir="rtl" class="form-control" name="city" id="city">
                        @foreach($cities as $Item)
                            <option value="{{ $Item->id }}">{{ $Item->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="InputFrame">
                    <label for="DescriptionPost">توضیحات محصول</label>
                    <textarea name="DescriptionPost" rows="10" class="form-control text-right"></textarea>
                </div>
            </div>

            <div class="ServicesPost">
                <div class="InputFrame">
                    <label for="Services">خدمات مجموعه</label>
                    <textarea name="Services" rows="5" class="form-control text-right"></textarea>
                </div>
            </div>
            <div class="BTNCollectionArea">
                <button class="CreateCollectionBTN btn btn-outline-success btn-block">ثبت اطلاعات</button>
            </div>
        </form>
    </div>
@endsection


