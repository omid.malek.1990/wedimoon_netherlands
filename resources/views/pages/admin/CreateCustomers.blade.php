@extends('pages.admin.dashboard')

@section('Items')
    <ul id="ListGroup" class="list-group ListGroupItems" item="{{ $operation }}">
        <li id="InsertOperation" class="list-group-item">
            <a href="{{ route('customers.create') }}">افزودن مشتری</a>
        </li>
        <li id="ListOperation" class="list-group-item">
            <a href="{{ route('customers.index') }}">لیست مشتریان</a>
        </li>
    </ul>
@endsection

@section('BaseSection')
    <div class="CreateCollectionPage">
        @includeIf('pages.error')
        <form action="{{ route('customers.store') }}" method="post">
            {{ csrf_field() }}
            <div class="CollectionInfo">
            </div>
            <div class="CollectionPost">
                <div class="InputFrame">
                    <label for="fullname">نام و نام خانوادگی</label>
                    <input type="text" class="form-control text-right" name="fullname"/>
                </div>
                <div class="InputFrame">
                    <label for="email">آدرس ایمیل</label>
                    <input type="text" class="form-control text-left" name="email"/>
                </div>
                <div class="InputFrame">
                    <label for="mobile">تلفن همراه</label>
                    <input type="text" class="form-control text-left" name="mobile"/>
                </div>
                <div class="InputFrame">
                    <label for="password">رمز عبور</label>
                    <input type="password" class="form-control text-left" name="password"/>
                </div>
                <div class="InputFrame">
                    <label for="city">نام شهر</label>
                    <select dir="rtl" class="form-control" name="city" id="city">
                        @foreach($cities as $city)
                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                        @endforeach
                    </select>
                </div>

                <button type="submit" class="CreateCollectionBTN btn btn-outline-success btn-block">ثبت اطلاعات</button>
            </div>

        </form>
    </div>
@endsection
