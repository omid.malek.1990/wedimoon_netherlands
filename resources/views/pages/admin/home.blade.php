@extends('pages.admin.dashboard')

@section('Items')
    <ul id="ListGroup" class="list-group ListGroupItems" item="{{ $operation }}">
        <li id="dashboard" class="list-group-item">
            <a href="{{ route('home') }}">داشبورد</a>
        </li>
    </ul>
@endsection

@section('BaseSection')

    <div class="SectionMajor table-responsive">
        <ul class="ItemsBoxFrame">
            <li class="customers">
                <a href="{{ route('customers.index') }}">
                    <h4>{{ $customers->first()->customers_count }}</h4>
                    <h5>کاربران</h5>
                </a>
            </li>
            <li class="users">
                <a href="{{ route('posts.index') }}">
                    <h4>{{ $users->first()->users_count }}</h4>
                    <h5>مجموعه ها</h5>
                </a>
            </li>
            <li class="comments">
                <a href="{{ route('manageComments.index') }}">
                    <h4>{{ $comments->first()->comments_count }}</h4>
                    <h5>نظرات</h5>
                </a>
            </li>
            <li class="images">
                <a href="{{ route('imagesInsert',['used'=>'index.app']) }}">
                    <h4>{{ $images->first()->images_count }}</h4>
                    <h5>تصاویر</h5>
                </a>
            </li>
        </ul>
    </div>
@endsection
