@extends('pages.admin.index')
@section('title')
    پنل مدیریت Wedimoon
@endsection
@section('content')
    <div class="FirstRow">
        <div class="SiteHeader">
            <div class="WellcomeMsg">{{ $info->fullname.' خوش آمدید ' }}</div>
            <div id="ItemsIcon" class="ItemsIcon">
                <i class="fas fa-bars"></i>
            </div>
            <div class="TitleDashboard">Wedimoon</div>
        </div>
    </div>
    <div id="NavigationRow" class="NavigationRow">
        <ul id="NavigationColumn" page="{{ $page }}" class="NavigationColumn">
            <li id="Home">
                <a href="{{ route('home') }}" class="">
                    <span class="fas fa-tachometer-alt"></span>
                    <span>صفحه اصلی</span>
                </a>
            </li>
            <li id="ChangePassword">
                <a href="{{ route('password') }}" class="">
                    <span class="fas fa-key"></span>
                    <span>تغییر رمز عبور</span>
                </a>
            </li>
            <li id="ChangeInformation">
                <a href="{{ route('profile.edit', session('id')) }}">
                    <span class="fas fa-user-edit"></span>
                    <span>اطلاعات مدیر</span>
                </a>
            </li>
            <li id="Collections">
                <a href="{{ route('posts.index') }}">
                    <span class="fas fa-clipboard-list"></span>
                    <span>مجموعه ها</span>
                </a>
            </li>
            <li id="Customers">
                <a href="{{ route('customers.index') }}">
                    <span class="fas fa-users"></span>
                    <span>کاربران</span>
                </a>
            </li>
            <li id="Comments">
                <a href="{{ route('manageComments.index') }}">
                    <span class="fas fa-comments"></span>
                    <span>نظرات</span>
                </a>
            </li>
            <li id="Images">
                <a href="{{ route('imagesInsert',['used'=>'index.app']) }}">
                    <span class="fas fa-images"></span>
                    <span>تصاویر</span>
                </a>
            </li>
            <li id="Exit">
                <a href="{{ route('admin.login') }}">
                    <span class="fas fa-sign-out-alt"></span>
                    <span>خروج</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="RowContent">
        <div class="ListItemsColumn">
            @yield('Items')
        </div>
        <div class="ContentColumn">
            @yield('BaseSection')
        </div>
    </div>
@endsection