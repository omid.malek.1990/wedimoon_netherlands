@extends('pages.admin.dashboard')


@section('Items')
    <ul id="ListGroup" class="list-group ListGroupItems" item="{{ $operation }}">
        <li id="Insert" class="list-group-item">
            <a href="{{ route('images.create') }}">افزودن تصویر مجموعه ها</a>
        </li>
        <li id="InsertLogo" class="list-group-item">
            <a href="{{ route('InsertSliderIndex') }}">افزودن اسلایدر صفحه اصلی </a>
        </li>
        <li id="List" class="list-group-item">
            <a href="{{ route('imagesInsert',['used'=>'index.app']) }}">لیست تصاویر</a>
        </li>
    </ul>
@endsection

@section('BaseSection')
    <div class="CreateCollectionPage">
        @includeIf('pages.error')
        <input type="hidden" name="path" value="{{ route('ComboCategories') }}" />
        <form action="{{ route('StoreImage') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="CollectionInfo">
                <div class="InputFrame">
                    <label for="PostTitle">دسته بندی مجموعه</label>
                    <select dir="rtl" class="form-control" name="categorie" id="UsersCategorie">
                        @foreach($categoriesUser as $Item)
                            <option value="{{ $Item->id }}">{{ $Item->title }}</option>
                        @endforeach
                    </select>
                </div>
                <div id="CollectionCombo" class="InputFrame">
                    <label for="PostTitle">عنوان مجموعه</label>
                    <select dir="rtl" class="form-control" name="title" id="InsertUsersCategorie">
                            <option value=""></option>
                    </select>
                </div>
            </div>
            <div class="CollectionPost">
                <div class="InputFrame text-right">
                    <label for="file">بارگذاری تصویر</label>
                    <input type="file" class="form-control" name="file" id="file" />
                </div>
                <div class="InputFrame">
                    <label for="Category">دسته بندی تصاویر</label>
                    <select dir="rtl" class="form-control" name="CategoryImages" id="CategoryImages">
                        @foreach($categoriesImages as $Item)
                            <option value="{{ $Item->used_in }}">{{ $Item->title }}</option>
                        @endforeach
                    </select>
                </div>

            </div>
            <div class="BTNCollectionArea">
                <button class="CreateCollectionBTN btn btn-outline-success btn-block">ثبت اطلاعات</button>
            </div>
        </form>
    </div>
@endsection


