@extends('pages.admin.dashboard')

@section('Items')
    <ul id="ListGroup" class="list-group ListGroupItems" item="{{ $operation }}">
        <li id="ListOperation" class="list-group-item">
            <a href="{{ route('manageComments.index') }}">لیست نظرات</a>
        </li>
    </ul>
@endsection

@section('BaseSection')

    <div class="ComboSection">
        <div class="input-group ImagesSelection">
            <form class="CategoryFilterForm" action="{{ route('manageComments.index') }}">
                <select class="custom-select text-right" id="CategorieImages">
                    @foreach($comboPosts as $post)
                        @if($post->id == request()->route('id'))
                            <option value="{{ $post->id }}" selected="selected">{{ $post->title }}</option>
                        @else
                            <option value="{{ $post->id }}">{{ $post->title }}</option>
                        @endif
                    @endforeach
                </select>
            </form>
        </div>
    </div>

    <div class="SectionMajor table-responsive">
        @if(empty($comments))
            <section  class="ErrorFrame">
                <section class="alert alert-primary text-center ErrorBox BMitra">
                    <span class="IRanSans ErrorMessage">{{ 'هیچ داده ای یافت نشد' }}</span>
                </section>
            </section>
        @else
            <table class="table">
                <thead class="thead-light">
                <tr>
                    <th>متن نظر</th>
                    <th class="MiddleColumn">ارسال کننده</th>
                    <th class="SmallColumn">وضعیت نمایش</th>
                    <th class="SmallColumn">عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($comments as $comment)
                    <tr>
                        <td>{{ $comment->text }}</td>
                        <td>{{ $comment->customer->fullname }}</td>
                        <td>{!! $comment->status !!}</td>
                        <td>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <button type="button" class="btn btn-outline-secondary dropdown-toggle"
                                            data-toggle="dropdown">
                                        انتخاب کنید
                                    </button>
                                    <div class="dropdown-menu">
                                        <a data-toggle="modal" data-target="#RemoveModal" class="dropdown-item DropDownItem"
                                           path="{{ route('manageComments.remove',$comment->id) }}"
                                           href="#">
                                            <span class="fas fa-trash-alt"></span>
                                            <span>حذف</span>
                                        </a>
                                        <a class="dropdown-item DropDownItem" href="{{ route('manageComments.change',$comment->id) }}">
                                            <span class="fas fa-edit DropdDownIcon"></span>
                                            <span class="">{{$comment->visible==0 ? 'نمایش نظر' : 'عدم نمایش نظر'}}</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            @if($comments->total()>$paginate)
                <div class="FooterPagination">
                    <span class="PaginationLinks">{{ $comments->links() }}</span>
                </div>
            @endif

            @includeIf('pages.admin.removeModalUser')
        @endif


    </div>
@endsection
