@extends('pages.admin.dashboard')

@section('Items')
    <ul id="ListGroup" class="list-group ListGroupItems" item="{{ $operation }}">
        <li id="Change" class="list-group-item">
            <a href="{{ route('password') }}">تغییر پسورد</a>
        </li>
    </ul>
@endsection


@section('BaseSection')
    <div class="ChangePasswordBox">
        @includeIf('pages.error')
        <form action="{{ route('password.update') }}" method="post">
            {{ csrf_field() }}
            <div class="CollectionInfo">
                <div class="InputFrame">
                    <label for="curr_pass">رمز عبور فعلی</label>
                    <input type="password" class="form-control" name="old_password"/>
                </div>
                <div class="InputFrame">
                    <label for="new_pass">رمز عبور جدید</label>
                    <input type="password" class="form-control" name="password"/>

                </div>
                <div class="InputFrame">
                    <label for="new_pass_confirmation">تکرار رمز عبور</label>
                    <input type="password" class="form-control" name="password_confirmation"/>
                </div>

                <button class="CreateCollectionBTN btn btn-outline-success btn-block">ثبت اطلاعات</button>
            </div>

            {{--<div class="BTNCollectionArea">--}}
            {{--<button class="CreateCollectionBTN btn btn-outline-success btn-block">ثبت اطلاعات</button>--}}
            {{--</div>--}}
        </form>
    </div>
@endsection
