@extends('main')
@section('title')
    صفحه ورود
@endsection
@section('content')
    <div class="FirstRow">
        <div class="LoginFormFrame">
            <h1 class="text-center">بازیابی رمز عبور</h1>

            @includeIf('pages.error')

            <form method="post" action="{{ route('reset.password') }}" class="">
                {{ csrf_field() }}
                <div class="InputsGroup input-group input-group-lg">
                    <input type="text" name="email" class="form-control text-left" placeholder="your email address"/><span
                            class="w-100"></span>
                </div>
                <div class="LoginFormBTN">
                    <input class="SignInBtn hvr-pulse" type="submit" value="بازیابی رمز عبور"/>
                </div>
                <div class="hvr-pulse ForgottenPassword">
                    <a class="" href="{{ route('admin.login') }}">ورود به سامانه</a>
                </div>
            </form>
        </div>
    </div>
    {{--https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js--}}

@endsection