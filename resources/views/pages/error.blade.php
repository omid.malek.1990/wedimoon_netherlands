@if(session()->has('class'))
    @if(count($errors)>0)
        <div class="ErrorFrame">
            <div class="alert {{ session()->get('class') }} text-center ErrorBox BMitra">
                @foreach($errors->all() as $error)
                    <span dir="rtl" class="BMitra ErrorMessage">{{ $error }}</span>
                @endforeach
            </div>
        </div>
    @endif
@else
    @if(count($errors)>0)
        <div class="ErrorFrame">
            <div class="alert alert-info text-center ErrorBox BMitra">
                @foreach($errors->all() as $error)
                    <span class="IRanSans ErrorMessage">{{ $error }}</span>
                @endforeach
            </div>
        </div>
    @endif
@endif