@if($status == "0")
    <span class="text-danger">غیرفعال</span>
@elseif($status == "1")
    <span class="text-success">فعال</span>
@else
    <span class="text-gray">تعریف نشده</span>
@endif
