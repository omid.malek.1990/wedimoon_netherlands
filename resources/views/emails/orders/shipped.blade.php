<html xmlns="http://www.w3.org/1999/xhtml" lang="en" charset="UTF-8">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/mail.css') }}"/>
</head>
<body dir="ltr">
<container>
    <section>
        <base-col>
            <sender>
                <b style=''>{!! '&nbsp;'.':'.'&nbsp;'.$from !!} </b>
                <p>{{ $fullname }}</p>
            </sender>

            <subject>
                <b>{!! '&nbsp;'.':'.'&nbsp;'.'موضوع ایمیل' !!}</b>
                <p> {{ $subject }} </p>
            </subject>
            <side class="MailContent">
                <username class="UsernameFrame">
                    <strong>{{ $username }}</strong>
                    <b>{!! ' نام کاربری : ' !!}</b>
                </username>
                <password class="PasswordFrame">
                    <strong>{{ $password }}</strong>
                    <b>{!! ' کلمه عبور : ' !!} </b>
                </password>
            </side>
        </base-col>
    </section>
</container>
</body>
</html>