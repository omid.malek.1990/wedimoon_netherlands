/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap')


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */




function LinkActivator() {
    var page = $("#NavigationColumn").attr("page");
    $(page).addClass('ActivePage');
}

function ItemsActivator() {
    var page = $("#ListGroup").attr("item");
    $(page).addClass('active');
}

function CollectionCombo(path, categorie_id, _token) {
    $.ajax({
        url: path,
        type: 'POST',
        data: {_token: _token, categorie_id: categorie_id},
        success: function (data) {

            console.log(data.data);
            if (data.error == 0) {
                $("#CollectionCombo select").remove('');
                $("#CollectionCombo").append(data.data);
            } else {
                // window.alert(data.error);
            }
        },
        error: function () {
            console.log("IsErorr");
        }
    });
}

$(document).ready(function () {
    $('div.alert').delay(3000).slideUp(1000);
    LinkActivator();
    ItemsActivator();

    var path = $('[name="path"]').val();
    var categorie_id = $("#UsersCategorie").val();
    var _token = $('input[name="_token"]').val();

    CollectionCombo(path, categorie_id, _token);
    $("#ItemsIcon").click(function () {
        $("#NavigationRow").slideToggle();
    });

    $("#CategoryImages").change(function(){
        var used_in=$(this).val();
        if(used_in=='icon.categorie') {
            $("#CollectionCombo select").slideUp();
        }else{
            $("#CollectionCombo select").slideDown();
        }
    });

});


$(document).ready(function () {
    $("body").delegate("#UsersCategorie", "change", function () {
        var path = $('[name="path"]').val();
        var categorie_id = $(this).val();
        var _token = $('input[name="_token"]').val();
        CollectionCombo(path, categorie_id, _token)
    });
    $("body").delegate("#CategorieImages", "change", function () {
        // var path=$('#Path').val();
        var categorie_id = $(this).val();
        var action = $(this).parent('form').attr('action');
        var arr=action.split('/');
        // arr[4] is used variable value from address url
        var urlArr=Array(arr[0],arr[1],arr[2],arr[3],categorie_id);
        var url=urlArr.join('/');
        window.location=url
    });

});


/*******************RemoveUserModal********************/

$(document).ready(function () {
    $("body").delegate('[data-target="#RemoveModal"]',"click",function(){
        var href=$(this).attr("path");
        $("#ConfirmRemove").attr("href",href);
        $('#RemoveModal').on('shown.bs.modal', function () {
            $('#myInput').trigger('focus');
        })

    })
})