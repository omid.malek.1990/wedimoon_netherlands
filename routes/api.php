<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('collections',API\UsersController::class);

Route::get('categories/posts/{categorie_id}/{citie_id}','API\CategorieController@posts');

Route::resource('categories',API\CategorieController::class);

Route::resource('tokens',API\ClientController::class);

Route::post('ShowLikeStatus','API\LikesController@LikeStatus');
Route::post('likes','API\LikesController@store');

Route::post('dislikes','API\LikesController@destroy');

Route::post('inCreaseScore','API\ScoresController@store');
Route::post('ShowScore','API\ScoresController@show');
Route::post('deCreaseScore','API\ScoresController@destroy');
Route::post('OtherScore','API\ScoresController@otherScores');


Route::post('addToBookmark','API\BookmarksController@store');
Route::post('ShowBookmarksList','API\BookmarksController@BookmarkList');
Route::post('ShowBookmarksStatus','API\BookmarksController@BookmarkStatus');
Route::post('RemoveBookmark','API\BookmarksController@destroy');

Route::post('searchPost','API\PostsController@search');
Route::get('specials_offer/{count}','API\PostsController@SpecialOffer')->name('specials_offer.posts');
Route::get('Favorites/{count}','API\PostsController@favorites')->name('favorite.posts');
Route::get('bestApp/{count}','API\PostsController@best')->name('best.posts');
Route::resource('posts',API\PostsController::class);

Route::resource('comments',API\CommentsController::class);

Route::resource('cities',API\CityController::class)->only(['index']);
Route::post('customers/iscustomer','API\CustomersController@iscustomer');
Route::post('customers/login','API\CustomersController@login');
Route::post('customerProfile','API\CustomersController@show');
Route::post('updateProfile','API\CustomersController@update');
Route::resource('customers',API\CustomersController::class)->except(['show','update']);

Route::get('sliders/{id}/{used_in}','API\SlidersController@show');



/*************************************/
