<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Auth::routes();

Route::get('/home', 'AdminController@index')->name('home');


Route::get('/', 'AdminController@login')->name('admin.login');
Route::post('/signin','AdminController@Signin')->name('admin.signin');
Route::get('/Exit', 'AdminController@SignOut')->name('sign.out');

Route::get('RemoveUser/{id}','UsersController@destroy')->name('user.remove');
Route::resource('collections',UsersController::class)->except(['destroy']);

Route::get('special_offer_check/{id}','PostsController@SpecialOffer')->name('posts.specialOffer');
Route::get('RemovePost/{id}','PostsController@destroy')->name('post.remove');
Route::resource('posts',PostsController::class)->except(['destroy']);

Route::get('imagesInsert/{used}','ImagesController@index')->name('imagesInsert');
Route::get('InsertIndexSlider','ImagesController@imageIndexSlider')->name('InsertSliderIndex');
Route::post('Storeindex','ImagesController@storeSliderIndex')->name('StoreSliderIndex');
Route::post('Storeimage','ImagesController@store')->name('StoreImage');

Route::get('RemoveImage/{id}','ImagesController@destroy')->name('image.remove');
Route::resource('images',ImagesController::class)->except(['index','store','destroy']);


//-- Change password and admin settings routes --

Route::get('ForgottenPassword','ChangePasswordController@forgottenPage')->name('forgotten.password');
Route::post('ChangePassword','ChangePasswordController@resetPassword')->name('reset.password');
Route::get('/password','ChangePasswordController@index')->name('password');
Route::post('/password','ChangePasswordController@update')->name('password.update');

Route::resource('/profile',ProfileSettingsController::class)->only(['edit', 'update']);
//Route::get('/profile','ProfileSettingsController@index')->name('profile');
//Route::post('/profile','ProfileSettingsController@update')->name('profile.update');

Route::get('RemoveCustomer/{id}','CustomersController@destroy')->name('customers.remove');
Route::resource('customers',CustomersController::class)->except(['destroy']);

Route::get('manageComments/{id?}', 'ManageCommentsController@index')->name('manageComments.index');
Route::get('RemoveComment/{id}','ManageCommentsController@destroy')->name('manageComments.remove');
Route::get('ChangeCommentStatus/{id}','ManageCommentsController@change')->name('manageComments.change');




Route::post('ComboCategorie','CategoriesController@GetCategoriesController')->name('ComboCategories');


