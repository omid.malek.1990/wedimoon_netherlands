<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    use HasApiTokens, Notifiable;
    protected $fillable=[
        'fullname', 'email', 'email_verified_at', 'password',
         'mobile','citie_id','active'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
    //     */
    protected $hidden = ['password'];


    public function  citie(){
        return $this->belongsTo(Citie::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }
    public function likes(){
        return $this->hasMany(Like::class);
    }

    public function scores(){
        return $this->hasMany(Score::class);
    }

    public function bookmarks(){
        return $this->hasMany(Bookmark::class);
    }
}
