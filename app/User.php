<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullname', 'email', 'email_verified_at', 'password',
        'phone', 'mobile', 'collection_title', 'long_itude','description',
        'late_itude', 'categorie_id', 'remember_token','citie_id','active',
        'address','services'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
    //     */
    protected $hidden = [
        'password', 'remember_token'
    ];


    public function categorie()
    {
        return $this->belongsTo(Categorie::class);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function citie(){
        return $this->belongsTo(Citie::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }


    public function likes(){
        return $this->hasMany(Like::class);
    }

    public function scores(){
        return $this->hasMany(Score::class);
    }

    public function bookmarks(){
        return $this->hasMany(Bookmark::class);
    }
}
