<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Admin;
use Illuminate\Support\Str;
use Mail;


class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $admin;

    public function __construct($admin)
    {
        $this->admin=$admin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
//        return $this->from('example@example.com')
//            ->view('emails.orders.shipped');
        $admin=$this->admin;
        $password=Str::random(5);
        return $this->view('emails.orders.shipped')->with([
                'fullname'=>'www.wedimoon.ir',
                'email'=>'omid.malek.1990@gmail.com',
                'subject'=>'بازیابی پسورد پنل مدیریت',
                'comment'=>'متن پیام',
                'from'=>'ارسال شده توسط',
                'password'=>$password,
                'username'=>$admin->username
            ]);
    }
}
