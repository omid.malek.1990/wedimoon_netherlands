<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Citie extends Model
{
    protected $fillable=[
        'name',
        'visible'
    ];

    public function posts(){
       return $this->hasMany(Post::class);
    }

    public function users(){
       return $this->hasMany(User::class);
    }


    public function customers(){
        return $this->hasMany(Customer::class);
    }

}
