<?php
/**
 * Created by PhpStorm.
 * User: OMID
 * Date: 11/21/2018
 * Time: 03:05 AM
 */

namespace App\Services\Collections;

use Illuminate\Support\Facades\Storage;
class File
{
    private $images='images';
    private $icons='icons';
    private $domain='http://www.wedimoon.ir';
    private $NotValid='داده معتبر نیست';
    public function ImageLink($NameFile){
        return $this->domain.'/'.$this->images.'/'.$NameFile;
    }
    public function IconLink($NameFile){
        return $this->domain.'/'.$this->icons.'/'.$NameFile;
    }

    public function GetActiveTitle($status){
        switch ($status){
            case 0:
                return 'غیر فعال';
                break;
            case 1:
                return 'فعال';
                break;
            default:
                return 'تعریف نشده';
                break;
        }
    }


    public function SubString($Content,$Number) {
        $ArrNews = explode(" ", $Content,$Number+2);
        $STR = "";
        $i = 0;
        foreach ($ArrNews as $Value) {
            $i++;
            $STR = $STR . $Value . " ";
            if ($i == $Number)
                break;
        }
        return $STR;
    }

    public function GetStatusStyle($status,$title){
        switch ($status){
            case 0:
                return '<span class="text-danger">'.$title.'</span>';
                break;
            case 1:
                return '<span class="text-success">'.$title.'</span>';
                break;
            default:
                return 'تعریف نشده';
                break;
        }
    }
    public function GetActiveTitleArr($arr){
        if(empty($arr) || count($arr)==0){
            return array('operationStatus'=>0,'response'=>$this->NotValid);
        }
        for ($i=0;$i<count($arr);$i++){
            $StatusTitle=$this->GetActiveTitle($arr[$i]->user->active);
            $arr[$i]->active=$this->GetStatusStyle($arr[$i]->user->active,$StatusTitle);
        }
        return array('operationStatus'=>1,'response'=>$arr);
    }

    public function GetActiveUserTitle($arr){

        if(empty($arr) || count($arr)==0){
            return array('operationStatus'=>0,'response'=>$this->NotValid);
        }
        for ($i=0;$i<count($arr);$i++){
            $StatusTitle=$this->GetActiveTitle($arr[$i]->active);
            $arr[$i]->active=$this->GetStatusStyle($arr[$i]->active,$StatusTitle);
        }
        return array('operationStatus'=>1,'response'=>$arr);
    }

    public function GetVisibleCommentsTitle($arr){
        if(empty($arr) || count($arr)==0 || json_encode([])==$arr){
            return array('operationStatus'=>0,'response'=>$this->NotValid);
        }
        for ($i=0;$i<count($arr);$i++){
            $StatusTitle=$this->GetActiveTitle($arr[$i]->visible);
            array_add($arr[$i],'status',$this->GetStatusStyle($arr[$i]->visible,$StatusTitle));
        }
        return array('operationStatus'=>1,'response'=>$arr);
    }

    public function RemoveImage($file_name){

        if($file_name !='noPhoto.jpg'){
            $path = $this->images.'/'.$file_name;
            unlink($path);
        }
        return 1;
    }
}