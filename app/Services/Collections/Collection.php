<?php

namespace App\Services\Collections;

use App\Post;
use App\User;

class  Collection
{
    public function getCollections()
    {
        return User::all();
    }

    public function ShowCollection($id)
    {
        return $user = User::find($id);
    }

    public function GetPostsCategorieCombo($categorie_id)
    {
        $posts = Post::where('categorie_id', '=', $categorie_id)->get();
        $combo = '';
        $combo .= '<select dir="rtl" class="form-control" name="title" id="InsertUsersCategorie">';
        foreach ($posts as $Item) {
            $combo .= '<option value="' . $Item->id . '">' . $Item->title . '</option>';
        }
        $combo .= '</select>';
        return $combo;
    }
}


?>