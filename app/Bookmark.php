<?php

namespace App;

use App\Http\Controllers\API\PostsController;
use Illuminate\Database\Eloquent\Model;

class Bookmark extends Model
{
    protected $fillable=['customer_id','post_id','user_id'];

    public function customer(){
        return $this->belongsTo(Customer::class);
    }

    public function post(){
        return $this->belongsTo(Post::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
