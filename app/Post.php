<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable=[
        'title',
        'description',
        'user_id',
        'categorie_id',
        'number_visited',
        'special_offer',
        'favorites',
        'visible',
        'citie_id'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function categorie(){
       return $this->belongsTo(Categorie::class);
    }

    public function citie(){
       return $this->belongsTo(Citie::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function likes(){
        return $this->hasMany(Like::class);
    }

    public function scores(){
        return $this->hasMany(Score::class);
    }

    public function bookmarks(){
        return $this->hasMany(Bookmark::class);
    }
}
