<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable=[
        'text',
        'customer_id',
        'post_id',
        'visible'
    ];

    public function customer(){
        return $this->belongsTo(Customer::class);
    }

    public function post(){
        return $this->belongsTo(Post::class);
    }
}
