<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable=[
        'src',
        'record_id',
        'used_in',
        'visible'
    ];
}
