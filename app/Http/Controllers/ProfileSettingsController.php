<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;
use Lang;
use Validator;

class ProfileSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Not supported!
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Not supported!
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Not supported!
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Not supported!
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $uid = $id;
        $admin = Admin::find($uid);

        return view('pages.admin.ProfileSettings')->with([
            'info' => $admin,
            'operation' => '#Change',
            'page' => '#ChangeInformation'
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'fullname' => 'required',
            'username' => 'required',
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {

            $result = Admin::where('id', $id)
                ->update([
                    'fullname' => $request->fullname,
                    'username' => $request->username,
                    'email' => $request->email
                ]);

            if (empty($result) || json_encode([]) == $result)
                return redirect()->back()->with(Lang::get('validation.OprationFailed'));
            else
                return redirect()->back()->withErrors(Lang::get('validation.successupdate'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Not supported!
    }

}
