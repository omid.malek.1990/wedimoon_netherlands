<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
 use App\Services\Collections\Collection;
class CategoriesController extends Controller
{

    protected $collection;

    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }


    public function GetCategoriesController(Request $request){
        $collections=$this->collection->GetPostsCategorieCombo($request->categorie_id);
        return response()->json(['error'=>0,'data'=>$collections],200);
    }
}
