<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class MailController extends Controller
{
    protected $user;

    private $sender = 'www.wedimoon.ir';
    private $from = 'info@wedimoon.ir';
    private $subject = 'بازیابی پسورد پنل مدیریت';
    private $comment;
private $password;

    private $base_url='http://www.wedimoon.ir';




    public function __construct($user,$new_password)
    {
        $this->user = $user;
        $this->password=$new_password;
    }

    public function mail_head()
    {
        $head = '';
        $head .= "<head>";
        $head .= "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>";
        $head .= "</head>";

        return $head;
    }

    public function CaracterSpace($Size)
    {
        $Text = "";
        for ($i = 0; $i < $Size; $i++) {
            $Text .= "&nbsp;";
        }
        return $Text;
    }


    public function mail_body()
    {

        $body = '';
        $body .= "<body dir=\"rtl\">";
        $body .= "<div style\"width:100%;\">";
        $body .= "<div style\"width:100%;\">";
        $body .= "<div style=\"width:600px;margin-top: 15%;min-height: 400px;font-family: Tahoma;font-size: 18px;\">";
        $body .= "<div style=\"border-radius: 5px;padding: 15px;background-color: #e6e6e6;display: flex;  margin-top: 10px;  flex-direction: row;\">";
        $body .= "<b style=\"text-align: right;\">" . $this->CaracterSpace(2) . "ارسال شده توسط" .$this->CaracterSpace(2) .":".$this->CaracterSpace(2) . "</b>";
        $body .= "<strong style=\"text-align: right;\">" . $this->sender . "</strong>";
        $body .= "</div>";

        $body .= "<div style=\"border-radius: 5px;padding: 15px;background-color: #e6e6e6;display: flex;  margin-top: 10px;  flex-direction: row;\">";
        $body .= "<b style=\"text-align: right;\">" . $this->CaracterSpace(2) . "موضوع ایمیل"  .$this->CaracterSpace(2) .":".$this->CaracterSpace(2) . "</b>";
        $body .= "<strong style=\"text-align: right;\">" . $this->subject . "</strong>";
        $body .= "</div>";
        $body .= "<div style=\"border-radius: 5px;padding: 15px;background-color: #e6e6e6;margin-top: 10px;  display: block;width: 100%;text-align: right;\">";
        $body .= "<div class=\"UsernameFrame\">";
        $body .= "<b style=\"display: inline-block;  text-align: right;  \">" . " نام کاربری " .$this->CaracterSpace(2) .":".$this->CaracterSpace(2) . "</b>";
        $body .= "<strong style=\"display: inline-block;  text-align: right;\">" . $this->user->username . "</strong>";
        $body .= "</div>";
        $body .= "<div class=\"PasswordFrame\">";
        $body .= "<b style=\"display: inline-block;  text-align: right;  \">" . ' کلمه عبور ' .$this->CaracterSpace(2) .":".$this->CaracterSpace(2) . "</b>";
        $body .= "<strong style=\"display: inline-block;  text-align: right; \">" . $this->password . "</strong>";
        $body .= "</div>";
        $body .= "</div>";
        $body .= "</div>";
        $body .= "</div>";
        $body .= "</div>";
        $body .= "</body>";

        return $body;
    }

    public function mail_html()
    {
        $html = '';
        $html .= "<html charset=\"UTF-8\">";
        $html .= $this->mail_head();
        $html .= $this->mail_body();
        $html .= "</html>";
        return $html;
    }

    public function send()
    {

        $headers='';
        $headers .= 'From: ' .$this->from. "\r\n" .'Reply-To: ' .$this->from . "\r\n";
        $headers  .= 'MIME-Version: 1.0' . "\r\n";
        $headers  .= "Content-Type: text/html; charset=UTF-8";

        $to = $this->user->email;
        $subject = $this->subject;
        $message = $this->mail_html();


        if (mail($to, $subject, $message, $headers)) {
            return 1;
        } else {
            return 0;
        }
    }
}
