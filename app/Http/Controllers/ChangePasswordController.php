<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth;
use Lang;
use Illuminate\Support\Str;
use App\Mail\OrderShipped;
use App\Http\Controllers\MailController;
use Mail;

class ChangePasswordController extends Controller
{
    /**
     * Display a view of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin = Admin::find(session()->get('id'));

        return view('pages.admin.ChangePassword', [
            'info' => $admin,
            'page' => '#ChangePassword',
            'operation' => '#Change'
        ]);
    }


    /**
     * Update the password settings in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required|same:password'
        ]);


        $current = $request->get('old_password');
        $new = $request->get('password');

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        $uid = session('id');

        $result = Admin::where('id', $uid)
            ->where('password', md5($current))
            ->update(['password' => md5($new)]);

        if (empty($result) || json_encode([]) == $result)
            return redirect()->back()->withErrors(Lang::get('validation.OprationFailed'));
        else
            return redirect()->back()->withErrors(Lang::get('validation.successupdate'));
    }

    public function forgottenPage()
    {
        return view('pages.admin.ForgottenPassword');
    }

    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:admins,email',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        } else {
            $email = $request->email;
            $password = Str::random(5);

            $admin = Admin::where('email', '=', $email)->first();
            $admin->update([
                'password'=>md5($password)
            ]);
            $mail = new MailController($admin,$password);


//            $to = "info@wedimoon.ir";
//            $subject = "omid malek";
//
//        $to      = 'info@wedimoon.ir';
//        $subject = 'the subject';
//        $message = 'hello';
//        $from="wedimoo1@wedimoon.ir";
//        $headers='';
//        $headers .= 'From: ' .$from. "\r\n" .'Reply-To: ' .$from . "\r\n";
//        $headers  .= 'MIME-Version: 1.0' . "\r\n";
//        $headers  .= "Content-Type: text/html; charset=iso-8859-1 ";
//
//        $retval = mail($to, $subject, $message, $headers);
//
//
//            if( $retval == true ) {
//                echo "Message sent successfully...";
//            }else {
//                echo "Message could not be sent...";
//            }

            if($mail->send()){
                return redirect()->back()->withErrors(Lang::get('validation.dataSend'));
            }else{
                return redirect()->back()->withErrors(Lang::get('validation.OprationFailed'));
            }
        }
    }

}
