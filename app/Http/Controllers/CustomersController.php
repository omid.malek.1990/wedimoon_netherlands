<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Bookmark;
use App\Citie;
use App\Comment;
use App\Customer;
use App\Like;
use App\Score;
use Illuminate\Support\Facades\DB;
use App\Services\Collections\File;
use App\Services\Collections\Token;
use Illuminate\Http\Request;
use Validator;
use Lang;

class CustomersController extends Controller
{
    private $paginate = 10;

    protected $file;

    protected $token;

    public function __construct(File $file,Token $token)
    {
        $this->file = $file;
        $this->token=$token;
    }

    /**
     * Display a listing of the customers.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin = Admin::find(session()->get('id'));

        $customers = Customer::select(['id', 'fullname', 'email', 'mobile', 'citie_id', 'active'])
            ->with(['citie:name,id'])
            ->orderBy('updated_at', 'DESC')
            ->paginate($this->paginate);

        return view('pages.admin.ListCustomers', [
            'info' => $admin,
            'page' => '#Customers',
            'operation' => '#ListOperation',
            'customers' => $customers,
            'paginate' => $this->paginate
        ]);
    }

    /**
     * Show the form for creating a new customer.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $admin = Admin::find(session()->get('id'));
        $cities = Citie::where('visible', '=', 1)->get(['id', 'name']);

        return view('pages.admin.CreateCustomers', [
            'info' => $admin,
            'page' => '#Customers',
            'operation' => '#InsertOperation',
            'cities' => $cities,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fullname' => 'required',
            'email' => 'required|email',
            'mobile' => 'required|numeric|min:10|unique:users,mobile',
            'password' => 'required|min:6'
        ]);

        if ($validator->fails()) {
            session()->flash('class', 'alert-danger');
            return back()->withErrors($validator->errors());
        } else {
            $customer = [
                'fullname' => $request->fullname,
                'email' => $request->email,
                'mobile' => $request->mobile,
                'password' => md5($request->password),
                'citie_id' => $request->city,
            ];

            $result = Customer::create($customer);

            if (empty($result) || $result == json_encode([])) {
                return redirect()->back()->withErrors(Lang::get('validation.OprationFailed'));
            } else {
                return redirect()->back()->withErrors(Lang::get('validation.successupdate'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Not supported!
    }

    /**
     * Show the form for editing the specified customer.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = Admin::find(session()->get('id'));

        $customer = Customer::where('id', '=', $id)->with(['citie:name,id'])->get();
        $cities = Citie::where('visible', '=', 1)->get(['id', 'name']);

        return view('pages.admin.EditCustomers', [
            'info' => $admin,
            'page' => '#Users',
            'operation' => '#EditOperation',
            'cities' => $cities,
            'customer' => $customer->first()
        ]);
    }

    /**
     * Update the specified customer in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'fullname' => 'required',
            'mobile' => 'required|numeric|min:10',
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            session()->flash('class', 'alert-danger');
            return back()->withErrors($validator->errors());
        } else {
            $result = Customer::where('id', $id)
                ->update([
                    'fullname' => $request->fullname,
                    'email' => $request->email,
                    'mobile' => $request->mobile,
                    'citie_id' => $request->city,
                ]);

            if(empty($result) || $result == json_encode([]))
                return redirect()->back()->withErrors(Lang::get('validation.OprationFailed'));
            else
                return redirect()->back()->withErrors(Lang::get('validation.successupdate'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // remove dependencies first
        $records=Comment::where('customer_id', $id)->get();

        if (!empty($records) || !(json_encode([]) == $records)) {
            for ($i = 0; $i < count($records); $i++) {
                $records[$i]->delete();
            }
        }



        $records=Like::where('customer_id', $id)->get();

        if (!empty($records) || !(json_encode([]) == $records)) {
            for ($i = 0; $i < count($records); $i++) {
                $records[$i]->delete();
            }
        }

        Score::where('customer_id', $id)->get();

        if (!empty($records) || !(json_encode([]) == $records)) {
            for ($i = 0; $i < count($records); $i++) {
                $records[$i]->delete();
            }
        }

        $bookmark=Bookmark::where('customer_id', $id)->get();

        if (!empty($bookmark) || !(json_encode([]) == $bookmark)) {
            for ($i = 0; $i < count($bookmark); $i++) {
                $ItemCode=$bookmark[$i]->delete();
            }
        }


        $scope='["customer-orders"]';


        $resultScope=DB::table('oauth_access_tokens')
            ->where('scopes','=',$scope)
            ->where('user_id','=',$id)->get();

        if (!empty($resultScope) || !(json_encode([]) == $resultScope) || count($resultScope)>0) {
            $resultScope=DB::table('oauth_access_tokens')
                ->where('scopes','=',$scope)
                ->where('user_id','=',$id)->delete();
        }

        // remove customer info
        Customer::where('id', $id)->delete();

        return back();
    }
}
