<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use App\Services\Collections\File;
use App\Admin;
use App\User;
use Validator;
use Lang;
use App\Citie;
use App\Categorie;
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $paginate = 10;

    protected $file;

    public function __construct(File $file)
    {
        $this->file = $file;
    }

    public function index()
    {
        $admin = Admin::find(session()->get('id'));
        $collection = User::select(['id','categorie_id','phone','collection_title','active'])
            ->where('active','=',0)
            ->with(['categorie:title,id'])->orderBy('updated_at','DESC')->paginate($this->paginate);


        $response = $this->file->GetActiveUserTitle($collection);
        if($response['operationStatus']==0){
            $collection='';
        }else{
            $collection=$response['response'];
        }
        return view('pages.admin.UsersList', [
            'info' => $admin,
            'page' => '#Collections',
            'operation' => '#ReservedList',
            'collections' => $collection,
            'paginate'=>$this->paginate
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**-
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = Admin::find(session()->get('id'));
        $user=User::where('id','=',$id)->with(['categorie:title,id', 'citie:name,id'])->get();
        $cities=Citie::where('visible','=',1)->get(['id','name']);
        $categories=Categorie::where('visible','=',1)->get(['id','title']);
        return view('pages.admin.EditUser',[
            'info' => $admin,
            'page' => '#Collections',
            'operation' => '#ReservedList',
            'cities'=>$cities,
            'categories'=>$categories,
            'post'=>$user->first()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator=Validator::make($request->all(),[
            'Fullname'=>'required',
            'Phone'=> 'required|numeric',
            'Mobile' => 'required|numeric|min:10',
            'Address' => 'required',
            'TitleCollection' => 'required',
            'Email'=> 'required|email',
            'PostTitle'=> 'required',
            'DescriptionPost'=> 'required',
            'Services'=> 'required',
            'Lateitude'=>'required',
            'Longitude'=>'required'
        ]);
        if ($validator->fails()){
            session()->flash('class', 'alert-danger');
            return back()->withErrors($validator->errors());
        }else{

            $phone=$request->Phone;
            $mobile=$request->Mobile;
            $user=User::where('id','!=',$id)->where(function ($query) use($phone,$mobile){
                $query->where('phone','=',$phone)->orWhere('mobile','=',$mobile);
            })->get();
            if (empty($user) || $user == json_encode([])){
                $UserData=[
                    'fullname'=>$request->Fullname,
                    'active'=>1,
                    'email'=>$request->Email,
                    'phone'=>$request->Phone,
                    'mobile'=>$request->Mobile,
                    'collection_title'=>$request->TitleCollection,
                    'address'=>$request->Address,
                    'services'=>$request->Services,
                    'citie_id'=>$request->city,
                    'categorie_id'=>$request->Category,
                    'long_itude' => $request->Longitude,
                    'late_itude' => $request->Lateitude,
                    'description'=>$request->input('DescriptionPost','بدون توضیحات')
                ];
                $PostData=[
                    'description'=>$request->DescriptionPost,
                    'title'=>$request->PostTitle,
                    'citie_id'=>$request->city,
                    'categorie_id'=>$request->Category,
                    'user_id'=>$id
                ];
                $post=Post::firstOrCreate($PostData);
                $olduser=User::where('id','=',$id)->get();
                $olduser->first()->update($UserData);
                $post->update($PostData);
                return redirect()->back()->withErrors(Lang::get('validation.successupdate'));
            }else{
                return redirect()->back()->withInput($request->all())->withErrors(Lang::get('validation.OprationFailed'));
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User::where('id','=',$id)->take(1)->get();
        $user->first()->delete();
        return back();
    }
}
