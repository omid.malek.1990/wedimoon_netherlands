<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Comment;
use App\Post;
use Lang;
use Illuminate\Http\Request;
use App\Services\Collections\File;

class ManageCommentsController extends Controller
{
    private $paginate = 10;

    /**
     * Display a listing of the comments.
     *
     * @param null $postId
     * @return \Illuminate\Http\Response
     */

    protected $file;

    public function __construct(File $file)
    {
        $this->file = $file;
    }




    public function index($postId=null)
    {
        $admin = Admin::find(session()->get('id'));

        // Fill posts combobox
        $posts = $this->getPostsForCombo();

        // Check if postId is set otherwise set it to first post available
        if (!isset($postId) || empty($postId))
            $postId = $posts->first()->id;

        $comments = Comment::select(['id', 'text', 'visible', 'customer_id'])
            ->where('post_id', $postId)
            ->orderBy('updated_at', 'DESC')
            ->with(['customer:id,fullname'])
            ->paginate($this->paginate);

        $response = $this->file->GetVisibleCommentsTitle($comments);
        if($response['operationStatus']==0){
            $comments='';
        }else{
            $comments=$response['response'];
        }
        return view('pages.admin.ListComments', [
            'info' => $admin,
            'page' => '#Comments',
            'operation' => '#ListOperation',
            'paginate' => $this->paginate,
            'comments' => $comments,
            'comboPosts' => $posts
        ]);
    }
    /**
     * Show the form for creating a new comment.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Not supported!
    }
    /**
     * Display the specified comment.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Not supported!
    }
    /**
     * Show the form for editing the specified comment.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Not supported!
    }
    /**
     * Remove the specified comment from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Comment::where('id', $id)->delete();

        return back();
    }
    /**
     * Changes the status of comment in storage
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function change($id)
    {
        $comment = Comment::where('id', $id)->first();
        $comment->visible = !$comment->visible;

        $result = $comment->save();

        if(empty($result) || $result == json_encode([]))
            return back()->withErrors(Lang::get('validation.OprationFailed'));
        else
            return back()->withErrors(Lang::get('validation.successupdate'));
    }
    /**
     * Get a list of available posts
     *
     * @return  List<Post>
     */
    private function getPostsForCombo()
    {
        return Post::select(['id','title'])
            ->orderBy('updated_at', 'desc')
            ->get();
    }

}
