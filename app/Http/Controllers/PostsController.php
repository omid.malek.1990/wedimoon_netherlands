<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use App\Services\Collections\File;
use App\Admin;
use App\User;
use Validator;
use Lang;
use App\Citie;
use App\Categorie;

use App\Bookmark;
use App\Comment;
use App\Image;
use App\Like;
use App\Score;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $paginate = 10;

    protected $file;

    public function __construct(File $file)
    {
        $this->file = $file;
    }


    public function index()
    {
        $admin = Admin::find(session()->get('id'));
        $collection = Post::select(['id', 'categorie_id', 'citie_id', 'user_id', 'special_offer'])
            ->with([
                'categorie:title,id', 'citie:name,id',
                'user:id,collection_title,categorie_id,citie_id,active'
            ])->orderBy('updated_at', 'DESC')->paginate($this->paginate);
        $response = $this->file->GetActiveTitleArr($collection);

        if ($response['operationStatus'] == 0) {
            $collection = '';
        } else {
            $collection = $response['response'];
        }

        return view('pages.admin.ListCollections', [
            'info' => $admin,
            'page' => '#Collections',
            'operation' => '#ListOperation',
            'collections' => $collection,
            'paginate' => $this->paginate
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $admin = Admin::find(session()->get('id'));
        $StatusOffer = array(0 => 'نمی باشد', 1 => 'می باشد');
        $cities = Citie::where('visible', '=', 1)->get(['id', 'name']);
        $categories = Categorie::where('visible', '=', 1)->get(['id', 'title']);
        return view('pages.admin.CreateCollections', [
            'info' => $admin,
            'page' => '#Collections',
            'operation' => '#InsertOperation',
            'cities' => $cities,
            'categories' => $categories,
            'offers' => $StatusOffer
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'Fullname' => 'required',
            'Phone' => 'required|numeric|unique:users,phone',
            'Mobile' => 'required|numeric|min:10|unique:users,mobile',
            'Address' => 'required',
            'TitleCollection' => 'required',
            'Email' => 'required|email',
            'PostTitle' => 'required',
            'DescriptionPost' => 'required',
            'Services' => 'required',
            'Lateitude' => 'required',
            'Longitude' => 'required'
        ]);
        if ($validator->fails()) {
            session()->flash('class', 'alert-danger');
            return back()->withErrors($validator->errors());
        } else {
            $UserData = [
                'fullname' => $request->Fullname,
                'email' => $request->Email,
                'phone' => $request->Phone,
                'mobile' => $request->Mobile,
                'collection_title' => $request->TitleCollection,
                'address' => $request->Address,
                'services' => $request->Services,
                'citie_id' => $request->city,
                'categorie_id' => $request->Category,
                'long_itude' => $request->Longitude,
                'late_itude' => $request->Lateitude,
                'description' => 'بدون توضیح'
            ];
            $user = User::create($UserData);
            if (empty($user) || $user == json_encode([])) {
                return redirect()->back()->withErrors(Lang::get('validation.OprationFailed'));
            } else {
                $PostData = [
                    'description' => $request->DescriptionPost,
                    'title' => $request->PostTitle,
                    'citie_id' => $request->city,
                    'categorie_id' => $request->Category,
                    'user_id' => $user->id
                ];

                $post = Post::create($PostData);

                return redirect()->back()->withErrors(Lang::get('validation.successupdate'));

            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = Admin::find(session()->get('id'));
        $post = Post::where('id', '=', $id)->with(['categorie:title,id', 'citie:name,id', 'user'])->get();
        $cities = Citie::where('visible', '=', 1)->get(['id', 'name']);
        $categories = Categorie::where('visible', '=', 1)->get(['id', 'title']);
        return view('pages.admin.EditCollections', [
            'info' => $admin,
            'page' => '#Collections',
            'operation' => '#ListOperation',
            'cities' => $cities,
            'categories' => $categories,
            'post' => $post->first()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'Fullname' => 'required',
            'Phone' => 'required|numeric',
            'Mobile' => 'required|numeric|min:10',
            'Address' => 'required',
            'TitleCollection' => 'required',
            'Email' => 'required|email',
            'PostTitle' => 'required',
            'DescriptionPost' => 'required',
            'Services' => 'required',
            'Lateitude' => 'required',
            'Longitude' => 'required'
        ]);
        if ($validator->fails()) {
            session()->flash('class', 'alert-danger');
            return back()->withErrors($validator->errors());
        } else {
            $post = Post::find($id);
            $phone = $request->Phone;
            $mobile = $request->Mobile;
            $user = User::where('id', '!=', $post->user_id)->where(function ($query) use ($phone, $mobile) {
                $query->where('phone', '=', $phone)->orWhere('mobile', '=', $mobile);
            })->get();
            if (empty($user) || $user == json_encode([])) {
                $UserData = [
                    'fullname' => $request->Fullname,
                    'email' => $request->Email,
                    'phone' => $request->Phone,
                    'mobile' => $request->Mobile,
                    'collection_title' => $request->TitleCollection,
                    'address' => $request->Address,
                    'citie_id' => $request->city,
                    'categorie_id' => $request->Category,
                    'long_itude' => $request->Longitude,
                    'late_itude' => $request->Lateitude,
                    'description' => 'بدون توضیح',
                    'services' => $request->Services
                ];

                $olduser = User::where('id', '=', $post->user_id)->update($UserData);
                $PostData = [
                    'description' => $request->DescriptionPost,
                    'title' => $request->PostTitle,
                    'citie_id' => $request->city,
                    'categorie_id' => $request->Category,
                    'user_id' => $id
                ];
                $post->update($PostData);
                return redirect()->back()->withErrors(Lang::get('validation.successupdate'));
            } else {
                return redirect()->back()->withErrors(Lang::get('validation.OprationFailed'));
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $user_id = $post->user_id;

        $bookmark = Bookmark::where('post_id', '=', $id)->get();
        if (!empty($bookmark) || !(json_encode([]) == $bookmark)) {
            for ($i = 0; $i < count($bookmark); $i++) {
                $ItemCode = $bookmark[$i]->delete();
            }
        }

        $records = Comment::where('post_id', '=', $id)->get(['id']);

        if (!empty($records) || !(json_encode([]) == $records)) {
            for ($i = 0; $i < count($records); $i++) {
                $records[$i]->delete();
            }
        }

        $records = Image::where('record_id', '=', $user_id)->get(['id']);

        if (!empty($records) || !(json_encode([]) == $records)) {
            for ($i = 0; $i < count($records); $i++) {
                $this->file->RemoveImage($records[$i]->src);
                $records[$i]->delete();
            }
        }


        $records = Like::where('post_id', '=', $id)->get(['id']);

        if (!empty($records) || !(json_encode([]) == $records)) {
            for ($i = 0; $i < count($records); $i++) {
                $records[$i]->delete();
            }
        }

        $records = Score::where('post_id', '=', $id)->get(['id']);

        if (!empty($records) || !(json_encode([]) == $records)) {
            for ($i = 0; $i < count($records); $i++) {
                $records[$i]->delete();
            }
        }

        $post->delete();
        User::destroy($user_id);


        return back();
    }

    public function SpecialOffer($id)
    {
        $validator = Validator::make(['id' => $id], [
            'id' => 'required|numeric|exists:posts,id',
        ]);

        if ($validator->fails()) {
            session()->flash('class', 'alert-danger');
            return back()->withErrors($validator->errors());
        } else {
            $post = Post::where('id', '=', $id)->first();
            if($post->special_offer==0){
                $post->special_offer=1;
            }
            else if($post->special_offer==1){
                $post->special_offer=0;
            }
            $post->save();
            return redirect()->route('posts.index');

        }
    }
}
