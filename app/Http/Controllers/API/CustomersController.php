<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Hash;
use App\Services\Collections\Token;
use PHPUnit\Test\Extension;
use App\Customer;
use App\Exceptions\Handler;
use Validator;
use App\Citie;
use Lang;
class CustomersController extends Controller
{


    protected $token;


    public function __construct(Token $token)
    {
        $this->token=$token;
    }

    public function iscustomer(Request $request,Handler $e){
        $validator = Validator::make($request->all(), [
            'mobile' => 'required|numeric',
            'client_id' => 'required|numeric|exists:oauth_clients,id',
            'client_password' => 'required|exists:oauth_clients,secret'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        } else {
            $MobileValidator=Validator::make($request->all(),[
                'mobile'=>'exists:customers,mobile'
            ]);

            if($MobileValidator->fails()){
                return response()->json([],204);
            }else{

                $password = md5($request->password);

                $customer = Customer::where('mobile', '=', $request->mobile)->where('active', '=',1)->firstOrFail();
                if($e instanceof ErrorException){
                    return response()->view('errors.500', [], 500);
                }
                if (isset($customer)) {
                    return response()->json($customer,200);
                } else {
                    return response()->json([],204);
                }
            }
        }
    }
    public function login(Request $request,Handler $e)
    {
        $validator = Validator::make($request->all(), [
            'mobile' => 'required|numeric|exists:customers,mobile',
            'password' => 'required',
            'client_id' => 'required|numeric|exists:oauth_clients,id',
            'client_password' => 'required|exists:oauth_clients,secret'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        } else {
            $password = md5($request->password);

             $customer = Customer::where('mobile', '=', $request->mobile)
                ->where('password', '=', $password)->where('active', '=',1)->get();

            if($e instanceof ErrorException){
                return response()->view('errors.500', [], 500);
            }
            if (count($customer) > 0) {
                $token=$this->token->getToken($customer->first()->id,$request->client_id,$request->client_password,'["customer-orders"]');
                array_add($customer,'api_token',$token->first()->id);
                return response()->json($customer,200);
            } else {
                return response()->json([],204);
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile' => 'required|numeric|min:10|unique:customers,mobile',
            'fullname'=>'required',
            'password'=>'required',
            'citie_id'=>'numeric|exists:cities,id',
            'client_id' => 'required|numeric|exists:oauth_clients,id',
            'client_password' => 'required|exists:oauth_clients,secret'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        } else {
            if(!isset($request->citie_id) || empty($request->citie_id)){
                $CitieName='مشهد';
                $citie=Citie::where('name','Like','%'.$CitieName.'%')->take(1)->get();
                $request->citie_id=$citie->first()->id;
            }
            $encrypted = md5($request->input('password'));
            $data = [
                'mobile' => $request->mobile,
                'password'=>$encrypted,
                'citie_id'=>$request->citie_id,
                'fullname'=>$request->fullname
            ];
            $customer = Customer::create($data);
            if(isset($customer)) {
                $token = $this->token->TokenCustomerCreator($customer->id);
                $token=$this->token->getToken($customer->id,$request->client_id,$request->client_password,'["customer-orders"]');
                array_add($customer,'api_token',$token->first()->id);
                return response()->json($customer, 200);
            }else{
                return response()->json([],500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required|exists:oauth_access_tokens,id',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        } else {
            $token = $request->token;
            $customer_id = $this->token->getUserId($token, '["customer-orders"]');;

            $data = [
                'customer_id' => $customer_id
            ];
            $record = Customer::select(['fullname','email','mobile','citie_id'])
                ->where('id','=',$customer_id->first()->user_id)->get();

            if (empty($record) || $record == json_encode([])) {

                return response()->json([
                    'erorr' => Lang::get('validation.NoData')
                ], 204);


            } else {

                $citie=Citie::find($record->first()->citie_id);
                if (!empty($record) || $record != json_encode([])){
                    array_add($record->first(),'citie_name',$citie->name);
                }

                return response()->json($record->first(), 200);

            }
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'city' => 'required|numeric|exists:cities,id',
            'fullname' => 'required',
            'mobile' => 'required|numeric|min:10',
            'token' => 'required|exists:oauth_access_tokens,id'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        } else {
            $token=$request->token;
            $customer_id = $this->token->getUserId($token, '["customer-orders"]');;
            $data = [
                'fullname' => $request->fullname,
                'mobile' => $request->mobile,
                'citie_id' => $request->city
            ];

            $mobile=$request->mobile;
            $SameMobilecustomer = Customer::select(['fullname','mobile','citie_id'])
                ->where('id','!=',$customer_id->first()->user_id)->where(function ($query) use($mobile){
                    $query->where('mobile','=',$mobile);
                })->first();

            if(empty($SameMobilecustomer) || json_encode([])==$SameMobilecustomer) {

                $customer = Customer::select(['id','fullname','mobile','citie_id'])
                    ->where('id','=',$customer_id->first()->user_id)->get();

                if (empty($customer) || json_encode([])==$customer) {
                    return response([], 204);
                } else {
                    $customer->first()->update($data);
                    return response()->json($customer->first(), 200);
                }
            }else{
                return response()->json([
                    'error'=>Lang::get('validation.ExistSameDatas')
                ],201);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
