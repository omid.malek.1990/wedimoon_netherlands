<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Services\Collections\File;
use App\Image;
class SlidersController extends Controller
{
    protected $file;
    public function __construct(File $file)
    {
        $this->file=$file;
    }

    public function show($id,$used_in){

        $images=Image::where('record_id','=',$id)->where('used_in','=',$used_in)->get(['id','src','record_id','used_in']);
        if(isset($images) && count($images)>0){
            for($i=0;$i<count($images);$i++){
                $link=$this->file->ImageLink($images[$i]->src);
                array_add($images[$i],'download_link',$link);
            }
            return response()->json($images,200);
        }else{
            return response()->json([],204);
        }

    }


}
