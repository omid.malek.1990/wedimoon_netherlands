<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Services\Collections\Token;
use App\Services\Collections\File;
use Validator;
use Lang;
use App\Post;
use App\User;
use App\Image;
use App\Categorie;
use App\Bookmark;

class BookmarksController extends Controller
{

    protected $token;
    protected $file;

    public function __construct(Token $token, File $file)
    {
        date_default_timezone_set("Iran");
        $this->token = $token;
        $this->file = $file;
    }


    public function BookmarkList(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|exists:oauth_access_tokens,id',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        } else {
            $customer_id = $this->token->getUserId($request->customer_id, '["customer-orders"]');


            $record = Bookmark::where('customer_id', '=', $customer_id->first()->user_id)->get(['id', 'post_id', 'user_id']);

            for ($i = 0; $i < count($record); $i++) {

                $user = User::where('id', '=', $record[$i]->user_id)->first();
                array_add($record[$i], 'collection_title', $user->collection_title);
                /***************add logo post***************/
                $image = Image::where('used_in', '=', 'logo.post')
                    ->where('record_id', '=', $record[$i]->post_id)->first();
                $download_link = $this->file->ImageLink($image->src);
                array_add($record[$i], 'download_link', $download_link);
//
                $categorie=Categorie::where('id','=',$user->categorie_id)->where('visible','=',1)
                    ->first();
//
                array_add($record[$i], 'categorie_title', $categorie->title);
            }


            if (empty($record) || $record == json_encode([])) {
                return response()->json([
                    'BookmarkNumber' => 0
                ], 200);
            } else {
                return response()->json($record, 200);
            }
        }
    }


    public function BookmarkStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|exists:oauth_access_tokens,id',
            'post_id' => 'required|numeric|exists:posts,id',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        } else {
            $customer_id = $this->token->getUserId($request->customer_id, '["customer-orders"]');
            $data = [
                'post_id' => $request->post_id,
                'customer_id' => $customer_id->first()->user_id
            ];


            $record = Bookmark::where('customer_id', '=', $data['customer_id'])
                ->where('post_id', '=', $data['post_id'])->get(['id']);


            if (empty($record) || $record == json_encode([])) {
                return response()->json([
                    'BookmarkStatus' => 0,
                    'erorr' => 'پست مذکور توسط کاربر پسندیده نشده است'
                ], 200);
            } else {
                return response()->json([
                    'BookmarkStatus' => 1,
                    'erorr' => 'پست مذکور توسط کاربر پسندیده شده است'
                ], 200);
            }
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|exists:oauth_access_tokens,id',
            'post_id' => 'required|numeric|exists:posts,id',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        } else {
            $customer_id = $this->token->getUserId($request->customer_id, '["customer-orders"]');
            $post = Post::where('id', '=', $request->post_id)->take(1)->get();
            $data = [
                'user_id' => $post->first()->user_id,
                'post_id' => $request->post_id,
                'customer_id' => $customer_id->first()->user_id
            ];

            $record = Bookmark::where('customer_id', '=', $data['customer_id'])
                ->where('post_id', '=', $data['post_id'])->get(['id']);


            if (empty($record) || $record == json_encode([])) {
                $bookmark = Bookmark::firstOrCreate($data);
                return response()->json([
                    'operation' => 1
                ], 200);
            } else {
                return response()->json([
                    'operation' => 0,
                    'erorr' => Lang::get('validation.ExistSameDatas')
                ], 200);
            }
        }
    }


    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|exists:oauth_access_tokens,id',
            'post_id' => 'required|numeric|exists:posts,id',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        } else {
            $customer_id = $this->token->getUserId($request->customer_id, '["customer-orders"]');

            $data = [
                'post_id' => $request->post_id,
                'customer_id' => $customer_id->first()->user_id
            ];

            $record = Bookmark::where('customer_id', '=', $data['customer_id'])
                ->where('post_id', '=', $data['post_id'])->get(['id']);


            if (empty($record) || $record == json_encode([])) {

                return response()->json([
                    'erorr' => Lang::get('validation.NoData')
                ], 204);


            } else {

                $record->first()->delete();
                return response()->json([
                    'operation' => 1,
                    'erorr' => 'رکورد مورد نظر حذف شد'
                ], 200);

            }
        }
    }
}
