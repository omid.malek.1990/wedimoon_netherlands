<?php

namespace App\Http\Controllers\API;

use App\Bookmark;
use App\Comment;
use App\Like;
use App\Score;
use App\User;
use foo\bar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exceptions\Handler;
use App\Services\Collections\File;
use App\Services\Collections\Token;
use App\Post;
use App\Image;
use Mockery\Exception;
use Validator;
use Lang;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $file;
    protected $token;

    public function __construct(File $file, Token $token)
    {
        $this->file = $file;
        $this->token = $token;
    }


    public function index(Handler $e)
    {
        $Post = Post::select([
            'posts.id', 'posts.title', 'posts.description',
            'posts.user_id', 'posts.categorie_id', 'posts.citie_id',
            'number_visited', 'posts.special_offer', 'posts.likes',
            'posts.score', 'posts.visible', 'posts.created_at', 'posts.updated_at',
            'images.src', 'images.id as image_id', 'images.used_in'
        ])->join('images', 'posts.id', '=', 'images.record_id')->where('images.used_in', '=', 'logo.post')->get();
        if ($e instanceof CustomException) {
            return response()->view('errors.custom', [], 500);
        }
        if (isset($Post) && count($Post) > 0) {
            for ($i = 0; $i < count($Post); $i++) {
                $link = $this->file->ImageLink($Post[$i]->src);
                array_add($Post[$i], 'download_link', $link);
            }
            return response()->json($Post, 200);
        } else {
            return response()->json([], 204);
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Post = Post::select([
            'posts.id', 'posts.title', 'posts.description',
            'posts.user_id', 'posts.categorie_id', 'posts.citie_id',
            'posts.number_visited', 'posts.special_offer', 'posts.likes',
            'posts.score', 'posts.visible', 'images.src', 'images.used_in',
            'users.long_itude', 'users.late_itude', 'users.services', 'users.address', 'users.phone',
            'users.mobile', 'users.fullname as manager'
        ])
            ->join('images', 'posts.id', '=', 'images.record_id')
            ->join('users', 'posts.user_id', '=', 'users.id')
            ->where('images.used_in', '=', 'logo.post')
            ->where('posts.id', '=', $id)->take(1)->get();

        if (empty($Post) || count($Post) <= 0 || json_encode([]) == $Post) {
            return response([], 204);
        } else {

            return response()->json($Post->first(), 200);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


    }

    public function favorites($count)
    {
        $validator = Validator::make(['count' => $count], [
            'count' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        } else {
            $collection = Post::select(['id', 'categorie_id', 'citie_id',
                'user_id', 'score', 'likes',
                'number_visited', 'special_offer', 'title'])
                ->with([
                    'categorie:title,id', 'citie:name,id',
                    'user:id,collection_title,fullname as manager,categorie_id,citie_id,active'])
                ->orderBy('likes', 'DESC')->take($count)->get();

            if (empty($collection) || $collection == json_encode([])) {

                return response()->json([
                    'erorr' => Lang::get('validation.NoData')
                ], 204);


            } else {
                /*****Add logo address******/
                for ($i = 0; $i < count($collection); $i++) {
                    $image = Image::where('used_in', '=', 'logo.post')->where('record_id', '=', $collection[$i]->id)
                        ->take(1)->get(['src']);
                    $link = $this->file->ImageLink($image->first()->src);
                    array_add($collection[$i], 'download_link', $link);
                }
                /***************************/
                return response()->json($collection, 200);

            }
        }
    }

    public function best($count)
    {
        $validator = Validator::make(['count' => $count], [
            'count' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        } else {
            $collection = Post::select(['id', 'categorie_id', 'citie_id',
                'user_id', 'score', 'likes',
                'number_visited', 'special_offer', 'title'])
                ->with([
                    'categorie:title,id', 'citie:name,id',
                    'user:id,collection_title,fullname as manager,categorie_id,citie_id,active'])
                ->orderBy('score', 'DESC')->take($count)->get();

            if (empty($collection) || $collection == json_encode([])) {

                return response()->json([
                    'erorr' => Lang::get('validation.NoData')
                ], 204);


            } else {
                /*****Add logo address******/
                for ($i = 0; $i < count($collection); $i++) {
                    $image = Image::where('used_in', '=', 'logo.post')->where('record_id', '=', $collection[$i]->id)
                        ->take(1)->get(['src']);
                    $link = $this->file->ImageLink($image->first()->src);
                    array_add($collection[$i], 'download_link', $link);
                }
                /***************************/
                return response()->json($collection, 200);

            }
        }
    }


    public function search(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'word' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        } else {
            $word = $request->word;
            $user = User::select([
                 'id','fullname', 'email', 'phone', 'mobile', 'collection_title', 'citie_id', 'categorie_id'
            ])->where('collection_title', 'LIKE', '%' . $word . '%')->with(['categorie:title,id', 'citie:name,id'])->get();

            if (empty($user) || $user == json_encode([])) {

                return response()->json([
                    'erorr' => Lang::get('validation.NoData')
                ], 204);


            } else {
                $posts=[];
                for ($i = 0; $i < count($user); $i++) {
                    $post = Post::select([
                        'images.src',
                        'posts.id'
                    ])
                        ->join('images', 'images.record_id', '=', 'posts.id')
                        ->where('posts.user_id', '=', $user[$i]->id)
                        ->where('images.used_in', '=', 'logo.post')->with('user:fullname,email,phone,mobile,collection_title,citie_id,categorie_id')
                        ->first();
                    $download_link = $this->file->ImageLink($post->src);
                    $user[$i]->id=$post->id;
                    array_add($user[$i], 'download_link', $download_link);
                }

                return response()->json($user, 200);

            }
        }
    }

    public function SpecialOffer($count)
    {
        $validator = Validator::make(['count' => $count], [
            'count' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        } else {
            $collection = Post::select(['id', 'categorie_id', 'citie_id',
                'user_id', 'score', 'likes',
                'number_visited', 'special_offer', 'title'])
                ->where('special_offer', '=', 1)->with([
                    'categorie:title,id', 'citie:name,id',
                    'user:id,collection_title,fullname as manager,categorie_id,citie_id,active'])
                ->orderBy('likes', 'DESC')->orderBy('updated_at', 'DESC')->take($count)->get();

            if (empty($collection) || $collection == json_encode([])) {

                return response()->json([
                    'erorr' => Lang::get('validation.NoData')
                ], 204);


            } else {
                /*****Add logo address******/
                for ($i = 0; $i < count($collection); $i++) {
                    $image = Image::where('used_in', '=', 'logo.post')->where('record_id', '=', $collection[$i]->id)
                        ->take(1)->get(['src']);
                    $link = $this->file->ImageLink($image->first()->src);
                    array_add($collection[$i], 'download_link', $link);
                }
                /***************************/
                return response()->json($collection, 200);

            }
        }
    }
}
