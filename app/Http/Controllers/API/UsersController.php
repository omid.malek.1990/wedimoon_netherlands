<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Services\Collections\Collection;
use App\Services\Collections\Token;
use App\Exceptions\Handler;
use Mockery\Exception;
use Validator;
use App\User;

class UsersController extends Controller
{


    protected $users;

    protected $token;

    public function __construct(Collection $collection, Token $token)
    {
        $this->token = $token;
        $this->users = $collection;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = $this->users->getCollections();
        return response()->json($data, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fullname' => 'required',
            'address' => 'required',
            'email' => 'email|unique:users,email',
            'phone' => 'required|numeric|min:10',
            'mobile' => 'required|numeric|min:10|unique:users,mobile',
            'collection_title' => 'required|max:78',
            'categorie_id' => 'required|numeric|exists:categories,id',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        } else {
            $encrypted = md5('123456789');
            $data = [
                'fullname' => $request->fullname,
                'email' => (isset($request->email) && !empty($request->email) ? $request->email : 'ذکر نشده است'),
                'phone' => $request->phone,
                'mobile' => $request->mobile,
                'collection_title' => $request->collection_title,
                'description' => (isset($request->description) && !empty($request->description) ? $request->description : 'بدون توضیحات'),
                'categorie_id' => $request->categorie_id,
                'address' => $request->address,
                'password' => $encrypted,
                'active' => 0,

            ];
            $user = User::create($data);
            if (isset($user)) {
                $token = $this->token->TokenCollectionCreator($user->id);
                return response()->json($user, 200);
            } else {
                return response([], 204);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $e = new Handler();
        $user = $this->users->ShowCollection($id);
        if ($e instanceof ErrorException) {
            return response()->view('errors.500', [], 500);
        }
        if (isset($user)) {
            return response()->json($user, 200);
        } else {
            return response()->json([], 204);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
