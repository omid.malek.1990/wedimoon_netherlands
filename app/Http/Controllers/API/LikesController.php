<?php

namespace App\Http\Controllers\API;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Collections\Token;
use App\Comment;
use Validator;
use Lang;

use App\Like;

class LikesController extends Controller
{

    protected $token;

    public function __construct(Token $token)
    {
        date_default_timezone_set("Iran");
        $this->token = $token;
    }


    public function LikeStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|exists:oauth_access_tokens,id',
            'post_id' => 'required|numeric|exists:posts,id',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        } else {
            $customer_id = $this->token->getUserId($request->customer_id, '["customer-orders"]');

            $data = [
                'post_id' => $request->post_id,
                'customer_id' => $customer_id->first()->user_id
            ];


            $record = Like::where('customer_id', '=', $data['customer_id'])
                ->where('post_id', '=', $data['post_id'])->get(['id']);


            if (empty($record) || $record == json_encode([])) {
                return response()->json([
                    'LikeStatus'=>0,
                    'erorr' => 'پست مذکور توسط کاربر پسندیده نشده است'
                ], 200);
            } else {
                return response()->json([
                    'LikeStatus'=>1,
                    'erorr' => 'پست مذکور توسط کاربر پسندیده شده است'
                ], 200);
            }
        }
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|exists:oauth_access_tokens,id',
            'post_id' => 'required|numeric|exists:posts,id',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        } else {
            $customer_id = $this->token->getUserId($request->customer_id, '["customer-orders"]');
            $post = Post::where('id', '=', $request->post_id)->take(1)->get();
            $data = [
                'user_id' => $post->first()->user_id,
                'post_id' => $request->post_id,
                'customer_id' => $customer_id->first()->user_id
            ];

            $record = Like::where('customer_id', '=', $data['customer_id'])
                ->where('post_id', '=', $data['post_id'])->get(['id']);


            if (empty($record) || $record == json_encode([])) {
                $like = Like::firstOrCreate($data);
                $post->first()->likes = ($post->first()->likes) + 1;
                $post->first()->save();
                return response()->json([
                    'LikeStatus'=>1
                ], 200);
            } else {
                return response()->json([
                    'LikeStatus'=>0,
                    'erorr' => Lang::get('validation.ExistSameDatas')
                ], 200);
            }
        }
    }


    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|exists:oauth_access_tokens,id',
            'post_id' => 'required|numeric|exists:posts,id',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        } else {
            $customer_id = $this->token->getUserId($request->customer_id, '["customer-orders"]');

            $data = [
                'post_id' => $request->post_id,
                'customer_id' => $customer_id->first()->user_id
            ];

            $record = Like::where('customer_id', '=', $data['customer_id'])
                ->where('post_id', '=', $data['post_id'])->get(['id']);


            if (empty($record) || $record == json_encode([])) {

                return response()->json([
                    'destroyStatusLike'=>0,
                    'erorr' => Lang::get('validation.NoData')
                ], 204);


            } else {

                $record->first()->delete();
                $post = Post::where('id', '=', $data['post_id'])->take(1)->get();
                if ($post->first()->likes > 0) {
                    $post->first()->likes = ($post->first()->likes) - 1;
                    $post->first()->save();
                }
                return response()->json([
                    'destroyStatusLike'=>1,
                    'erorr' => 'رکورد مورد نظر حذف شد'
                ], 200);

            }
        }
    }
}
