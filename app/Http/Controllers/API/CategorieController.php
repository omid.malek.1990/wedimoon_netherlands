<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Support\Facades\Storage;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Collections\File;
use App\Categorie;
use App\Post;
use Lang;
use App\Image;

class CategorieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $file;

    public function __construct(File $file)
    {
        $this->file = $file;
    }


    public function index()
    {
        $categories = Categorie::select(['images.src', 'images.used_in', 'categories.id as categorie_id', 'categories.title as categorie_title'])
            ->join('images', 'categories.id', '=', 'images.record_id')
            ->where('images.used_in', '=', 'icon.categorie')->get();
        if (isset($categories)) {
            return response()->json($categories, 200);
        } else {
            return response()->json([], 204);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function posts($categorie_id, $citie_id)
    {
        $validator = Validator::make(['categorie_id' => $categorie_id, 'citie_id' => $citie_id], [
            'categorie_id' => 'required|numeric|exists:categories,id',
            'citie_id' => 'required|numeric|exists:cities,id'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $PostsCategorie = Post::join('users', 'posts.user_id', '=', 'users.id')
            ->select([
                'posts.id', 'posts.title', 'posts.description', 'posts.user_id', 'posts.categorie_id',
                'posts.citie_id', 'posts.number_visited', 'posts.special_offer', 'posts.likes',
                'users.collection_title', 'users.long_itude', 'users.late_itude'
            ])
            ->where('posts.categorie_id', '=', $categorie_id)
            ->where('posts.citie_id', '=', $citie_id)->where('posts.visible', '=', 1)->get();

//        $folder = 'images';
//        $base_file = 'noPhoto.jpg';
//        $image->getClientOriginalExtension()
//        $base_path = public_path() . '/' . $folder . '/' . 'noPhoto.jpg';

//        $Arr = explode('.', $base_file);
//        $extension = $Arr[1];
//        $new_file = time() . rand(10, 1000) . '.' . $extension;
//        $new_path = public_path() . '/' . $folder . '/' . $new_file;
//        Storage::copy($base_path,$new_path);

//        $file=Storage::get(public_path('images\noPhoto.jpg'));
//        Storage::copy(public_path('/images/noPhoto.jpg'), public_path('/images/'.$new_file));
//        return response()->json($new_path, 200);

//        return response()->json($PostsCategorie, 200);
        if (empty($PostsCategorie) || json_encode([]) == $PostsCategorie) {
            return response()->json([], 204);
        } else {

            for ($i = 0; $i < count($PostsCategorie); $i++) {
                $logo = Image::where('record_id', '=', $PostsCategorie[$i]->id)
                    ->where('used_in', '=', 'logo.post')->take(1)->get(['src']);
                if (empty($logo) || json_encode([]) == $logo) {
                    $logo = Image::firstOrCreate([
                        'src' => 'noPhoto.jpg',
                        'record_id' => $PostsCategorie[$i]->user_id,
                        'used_in' => 'logo.post'
                    ]);

                    $AddressLogo = $this->file->ImageLink($logo->src);
                    array_add($PostsCategorie[$i], 'logo', $AddressLogo);
                } else {
                    $AddressLogo = $this->file->ImageLink($logo->first()->src);
                    array_add($PostsCategorie[$i], 'logo', $AddressLogo);
                }
            }

            return response()->json($PostsCategorie, 200);
        }
    }
}
