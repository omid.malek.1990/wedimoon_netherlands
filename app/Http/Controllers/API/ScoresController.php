<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Services\Collections\Token;
use App\Score;
use App\Post;
use Validator;
use Lang;
class ScoresController extends Controller
{
    protected $token;

    public function __construct(Token $token)
    {
        date_default_timezone_set("Iran");
        $this->token = $token;
    }
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|exists:oauth_access_tokens,id',
            'post_id' => 'required|numeric|exists:posts,id',
            'score'=>'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        } else {
            $customer_id = $this->token->getUserId($request->customer_id, '["customer-orders"]');
            $post = Post::where('id', '=', $request->post_id)->take(1)->get();
            $data = [
                'user_id'=>$post->first()->id,
                'post_id' => $request->post_id,
                'customer_id' => $customer_id->first()->user_id,
                'score'=>$request->score
            ];

            $record = Score::where('customer_id', '=', $data['customer_id'])
                ->where('post_id', '=', $data['post_id'])->get(['id','score']);

            if (empty($record) || $record == json_encode([])) {
                $like = Score::firstOrCreate($data);
                $post->first()->score = ($post->first()->score) + $request->score;
                $post->first()->save();
                return response()->json($post, 200);
            } else {
                $score = ((int)$post->first()->score) - ((int)$record->first()->score);
                if($score>=0){
                    $post->first()->score=$score;
                    $post->first()->save();
                }
                $score = ($post->first()->score) + $request->score;
                $post->first()->score=$score;
                $post->first()->save();
                $record->first()->update($data);
                return response()->json($post, 200);
            }
        }
    }

    public function show(Request $request){
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|exists:oauth_access_tokens,id',
            'post_id' => 'required|numeric|exists:posts,id',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        } else {
            $customer_id = $this->token->getUserId($request->customer_id, '["customer-orders"]');

            $data = [
                'post_id' => $request->post_id,
                'customer_id' => $customer_id->first()->user_id
            ];

            $record = Score::where('customer_id', '=', $data['customer_id'])
                ->where('post_id', '=', $data['post_id'])->take(1)->get(['score']);

            if (empty($record) || $record == json_encode([])) {
                return response()->json([], 204);
            } else {
                return response()->json([
                    'score'=> $record->first()->score,
                ], 200);
            }
        }
    }


    public function otherScores(Request $request){
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|exists:oauth_access_tokens,id',
            'post_id' => 'required|numeric|exists:posts,id',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        } else {
            $customer_id = $this->token->getUserId($request->customer_id, '["customer-orders"]');

            $data = [
                'post_id' => $request->post_id,
                'customer_id' => $customer_id->first()->user_id
            ];

            $record=DB::table('scores')
                ->where('customer_id','!=',$customer_id->first()->user_id)
                ->sum('score');




            if (empty($record) || $record == json_encode([])) {
                return response()->json([
                    'score'=>0
                ], 200);
            } else {
                return response()->json([
                    'score'=> $record,
                ], 200);
            }
        }
    }

    public function destroy(Request $request){
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|exists:oauth_access_tokens,id',
            'post_id' => 'required|numeric|exists:posts,id',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        } else {
            $customer_id = $this->token->getUserId($request->customer_id, '["customer-orders"]');

            $data = [
                'post_id' => $request->post_id,
                'customer_id' => $customer_id->first()->user_id
            ];

            $record = Score::where('customer_id', '=', $data['customer_id'])
                ->where('post_id', '=', $data['post_id'])->get(['id','score']);


            if (empty($record) || $record == json_encode([])) {

                return response()->json([
                    'erorr' => Lang::get('validation.NoData')
                ], 204);


            } else {

                $post = Post::where('id', '=', $data['post_id'])->take(1)->get();
                if ($post->first()->score > 0) {
                    $score = ((int)$post->first()->score) - ((int)$record->first()->score);
                    if($score>=0){
                        $post->first()->score=$score;
                        $post->first()->save();
                    }


                }
                $record->first()->delete();
                return response()->json([
                    'operation'=>1,
                    'erorr'=>'رکورد مورد نظر حذف شد'
                ], 200);

            }
        }
    }



}
