<?php

namespace App\Http\Controllers\API;

use App\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Services\Collections\Token;
use App\Comment;
use Validator;
use Lang;

class CommentsController extends Controller
{
    protected $token;
    public function __construct(Token $token)
    {
        $this->token=$token;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'text'=>'required',
            'customer_id'=>'required|exists:oauth_access_tokens,id',
            'post_id'=>'required|numeric|exists:posts,id',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),400);
        }else{
            $customer_id=$this->token->getUserId($request->customer_id,'["customer-orders"]');
            if(count($customer_id)>0){
                $data=[
                    'text'=>$request->text,
                    'post_id'=>$request->post_id,
                    'customer_id'=>$customer_id->first()->user_id,
                    'visible'=>1
                ];
                $comment=Comment::create($data);
                if(isset($comment)){
                    return response($comment,200);
                }else{
                    return response([],500);
                }
            }else{
                return response([],400);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comments=Comment::select(['comments.text','comments.id','comments.created_at','customers.fullname'])
            ->join('customers','comments.customer_id','=','customers.id')->where('post_id','=',$id)->get();

        if(empty($comments) || json_encode([])==$comments){
            return response()->json([],201);
        }else{
            return response()->json($comments,200);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment=Comment::find($id);
        if(isset($comment)){
            $comment=$comment->delete();
            return response()->json($comment,200);
        }else{
            return response()->json([],204);
        }
    }
}
