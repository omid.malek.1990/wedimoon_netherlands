<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Admin;
use Validator;
use Lang;
use App\Comment;
use App\Customer;
use App\User;


class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function login()
    {
        return view('pages.login');
    }

    public function Signin(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'username' => 'required|exists:admins,username',
            'password' => 'required'
        ]);
        $password = md5($request->password);
        if ($validator->fails()) {
            session()->flash('class', 'alert-danger');
            return back()->withErrors($validator->errors())->withInput(['username']);
        } else {
//            return $request->all();
            $password = md5($request->password);
            $admin = Admin::where('username', '=', $request->username)
                ->where('password', '=', $password)
                ->take(1)->get(['id', 'fullname']);
            if (empty($admin) || json_encode([]) == $admin) {
                session()->flash('class', 'alert-danger');
                return back()->withErrors(Lang::get('validation.PasswordNotValidate'));
            } else {
                $AdminID = $admin->first()->id;
                session(['id' => $AdminID]);
                return redirect()->route('home');
            }
        }
    }

    public function SignOut()
    {
        session()->flush();
        return redirect()->route('admin.login');
    }

    public function TableCount($table_name)
    {
        $table = DB::table($table_name)->select(DB::raw('count(*) as ' . $table_name . '_count'))->get();
        return $table;
    }


    public function index()
    {
        $info_admin = Admin::find(session()->get('id'));
        $comments = $this->TableCount('comments');
        $users = $this->TableCount('users');
        $customers = $this->TableCount('customers');
        $images = $this->TableCount('images');
        return view('pages.admin.home', [
            'info' => $info_admin,
            'page' => '#Home',
            'comments'=>$comments,
            'users'=>$users,
            'customers'=>$customers,
            'images'=>$images,
            'operation' => '#dashboard',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
