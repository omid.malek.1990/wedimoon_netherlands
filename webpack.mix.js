const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//    .sass('resources/sass/app.scss', 'public/css').options({
//     processCssUrls: false
// });
//
//
// mix.scripts([
//     'node_modules/jquery/dist/jquery.js',
//     'public/js/CustomiseScripts.js'
// ], 'public/js/all.js').version();


/**************************/

mix.js('resources/js/AdminApp.js','public/js')
    .sass('resources/sass/admin.scss', 'public/css').options({
    processCssUrls: false
});

mix.scripts([
    'node_modules/jquery/dist/jquery.js',
    'resources/js/AdminScripts.js'
], 'public/js/alladmin.js').version();



// mix.sass('resources/sass/mail.scss', 'public/css').options({
//     processCssUrls: false
// });